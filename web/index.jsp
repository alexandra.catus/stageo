
<%--
    Author     : AC
--%>

<%@page import="jdbc.Connexion"%>
<%@page import="jdbc.Config"%>
<%@page import="com.stageo.model.UtilisateurDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="connexion" scope="application" class="jdbc.Connexion"></jsp:useBean>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <title>Stages | Rosemont</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">       <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css">
        <link rel="stylesheet" href="css/style.css">
        
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <script src="scripts/scripts.js"></script>

    </head>
    <body>

        <nav class="navbar  fixed-top navbar-expand-lg navbar-light bg-light " role="navigation" id="menu">
            <a class="navbar-brand" href="?tache=afficherPageAccueil">
                <img  class="d-inline-block align-top" id="logo" src="./images/logoCouper.png"/>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-collapse-main" aria-controls="navbar-collapse-main" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbar-collapse-main">
                
                    <c:if test="${empty sessionScope.connecte}">
                        <ul class="navbar-nav">
                             <form id="formConnection" class="form-inline my-2 my-lg-0 mr-auto" action="*.do?tache=afficherPageInscription" method = "post" >
                                <button type="submit" class="btn my-btn-outline-secondary my-2 my-sm-0">Nous rejoindre</button>
                            </form>
                      
                        </ul>
                        </c:if>
                <c:choose>
                    <c:when test="${!empty sessionScope.connecte}">
                        <ul class="navbar-nav"
                            <li class="nav-item">
                                <a class="nav-link" href="*.do?tache=afficherPageProfil">Profil</a>
                            </li>
                            
                            <c:if test="${!sessionScope.role.equals('coordonateur')}">
                                <li class="nav-item">
                                    <a class="nav-link" href="*.do?tache=afficherPageCandidaturesEtudiant">Candidatures</a>
                                </li>
                            </c:if>
                                <li class="nav-item">
                                    <a class="nav-link" href="*.do?tache=afficherPageListeStage">Offres de stage</a>
                                </li>
                            <li class="nav-item">
                                <a class="nav-link" href="*.do?tache=afficherPageDocument">Documents</a>
                            </li>
                            <c:if test="${!sessionScope.role.equals('etudiant')}">
                                <li class="nav-item">
                                    <a class="nav-link" href="*.do?tache=afficherPageListeStagiaires">Recherche Étudiant</a>
                                </li>
                            </c:if>
                            <li class="nav-item">
                                <a class="nav-link" href="*.do?tache=effectuerDeconnexion">Déconnexion</a>
                            </li>
                        </ul>
                    </c:when>
                    <c:otherwise>
                         <ul class="navbar-nav ml-auto">
                            <form id="formConnection" class="form-inline my-2 my-lg-0 mr-auto" action="*.do?tache=effectuerConnexion" method = "post" >
                                <input type="email" class="form-control mr-sm-2" name="courrielConnexion" placeholder="Courriel" required>
                                <input type="password" class= "form-control mr-sm-2"  name="motDePasseConnexion" placeholder="Mot de passe" required>
                                <button type="submit" class="btn my-btn-outline-primary my-2 my-sm-0"> Se connecter</button>
                            </form>
                        </ul>
                    </c:otherwise>
                </c:choose>


            </div>

        </nav>



        <c:choose>
            <c:when test="${ !empty requestScope.vue }">
                <c:set var="vue" value="/WEB-INF/vue/${requestScope.vue}"/>
                <jsp:include page="${vue}" ></jsp:include>
            </c:when>
            <c:otherwise>
                <jsp:include page="/WEB-INF/vue/page-accueil.jsp"></jsp:include>
            </c:otherwise>
        </c:choose>
    </body>
</html>