<%-- 
    Document   : ListeStagesEleves
    Created on : Dec 6, 2018, 10:26:06 AM
    Author     : JP
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.List"%>
<%@page import="jdbc.Connexion"%>
<%@page import="jdbc.Config"%>
<%@page import="com.stageo.model.*"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<jsp:useBean id="dao" class="com.stageo.model.StageDAO">
    <jsp:setProperty name="dao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>
<c:set var="listeStage" value="${dao.findAll()}"/>
<jsp:useBean id="udao" class="com.stageo.model.UtilisateurDAO">
    <jsp:setProperty name="udao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<jsp:useBean id="cdao" class="com.stageo.model.CompagnieDAO">
    <jsp:setProperty name="cdao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<jsp:useBean id="odao" class="com.stageo.model.StageDAO">
    <jsp:setProperty name="odao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<jsp:useBean id="edao" class="com.stageo.model.EmployeurDAO">
    <jsp:setProperty name="edao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<jsp:useBean id="daoCritere" scope="page" class="com.stageo.model.CritereDAO">
    <jsp:setProperty name="daoCritere" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<c:set  var="listeCriteres" value="${daoCritere.findAll()}"/>

<div class="container" >



    <div class="row" >

        <div class="col-md-7">
            <div class="card">
                <div class="card-header titreProfil2">
                    <h1 class="card-title ">Rechercher Stage</h1>
                </div>
                <!-- Section de recherche des stages -->
                <!-- Barre de recherche -->
                <div class="card-body">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <label class="input-group-text"><i class="fas fa-search"></i></label>
                        </div>
                        <input id="inputRecherche" type="text" class="form-control" placeholder="Recherche par mot-clef...">
                    </div>
                    <!-- Fin de la barre de recherche -->

                    <div   id='sectionCompetences'>
                        <!-- bouton pour la liste -->
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <label class="input-group-text">
                                    <button class="btn my-btn-outline-secondary dropdown-toggle main-input" type="button" data-toggle="dropdown"> Filtrer par Compétences <span class="caret"></span></button>
                                    <ul class="dropdown-menu" id="liste">
                                        <c:forEach items="${listeCriteres}" varStatus="loop" var="critereRecherche">
                                            <li class="elementCritere"name="${critereRecherche.nom}">${critereRecherche.nom}
                                            </li>
                                        </c:forEach>
                                    </ul>
                                </label>
                            </div>
                            <!-- Conteneur de comperence -->
                            <div class="col-sm-8" >
                                <div id="conteneurCompetences">

                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Fin de section de recherche par competances -->


                    <div class="">
                        <table id="table" class="table table-striped table-hover borderTable">
                            <thead>
                                <tr>
                                    <td>Nom Compagnie <a href="#" class="fa fa-arrows-alt-v"></a></td>
                                    <td>Titre <a href="#" class="fa fa-arrows-alt-v"></a></td>
                                    <td>Remunération <a href="#" class="fa fa-arrows-alt-v"></a></td>
                                    <td>Statut candidature<a href="#" class="fa fa-arrows-alt-v"></a></td>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach  varStatus="loop" items="${listeStage}" var="stage">
                                    <fmt:formatDate var="dateDebut" value="${stage.dateDebut}" pattern="yyyy-MM-dd" />
                                    <fmt:formatDate var="dateFin" value="${stage.dateFin}" pattern="yyyy-MM-dd" />
                                    <c:set var="employeur"  value="${edao.read(stage.idEmployeur)}"/>
                                    <c:set var="compagnieid"  value="${employeur.compagnie.idCompagnie}"/>
                                    <c:set var="compagnie"  value="${cdao.read(compagnieid)}"/>
                                    <c:set var="criteresStage"  value="${daoCritere.findByOffre(stage.idOffre)}"/>
                                    <tr id="${stage.idOffre}">
                                        <td id="${stage.titre}">${compagnie.nom}</td>
                                        <td id="${stage.titre}">${stage.titre}</td>
                                        <td hidden id="${stage.description}"></td>
                                        <td hidden>${dateDebut}</td>
                                        <td hidden>${dateFin}</td>
                                        <td hidden>${stage.lienWeb}</td>
                                        <td hidden>${stage.lienDocument}</td>
                                        <td>${stage.nbVues}</td>
                                        <td hidden>${stage.fichier}</td>
                                        <td hidden>${stage.idOffre}</td>
                                        <td>${stage.remunere}</td>
                                        <td hidden>${compagnie.adresse.toString()}</td>
                                        <c:forEach  items="${criteresStage}" var="crit">
                                            <td name="crit" hidden>${crit.nom}</td>
                                        </c:forEach>
                                    </tr>

                                </c:forEach>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
        <!-- Fin de la section de recherche des stages -->
        <div class="col-md-5" >
            <div class='card'>
                <div class="card-header titreProfil">
                    <h1>Détails de l'offre</h1>
                    <small muted><span id='nbVues'>0</span> candidatures</small>
                </div>
                <fieldset class="card-body">
                    <h2 class="red"> Compagnie</h2>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <label class="input-group-text"> Nom Compagnie</label>
                        </div>
                        <input type="text"  name="titre" class="form-control" id="nomCompagnie" value="1"  placeholder="Non fonctionnel" disabled>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <label class="input-group-text"> Infos Compagnie</label>
                        </div>
                        <input type="text"  name="titre" class="form-control" id="infoCompagnie" value="1"  placeholder="Non fonctionnel" disabled>
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <label class="input-group-text"> Adresse Compagnie</label>
                        </div>
                        <input type="text"  name="titre" class="form-control" id="adresseCompagnie" value="1"  placeholder="Non fonctionnel" disabled>
                    </div>

                    <h2 class="red mb-3 inline"> Offre de stage <button id="fichier" class="btn my-btn-outline-secondary"> <i class="fas fa-file-download"></i></button>
                    </h2>
                    <div class="form-horizontal"> 
                        <div class="form-row">
                            <div class="form-group ">

                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <label class="input-group-text">Titre :</label>
                                    </div>
                                    <input type="text"  name="titre" class="form-control" id="titre" value="1"  placeholder="Titre" disabled>
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <label class="input-group-text">Description :</label>
                                    </div>
                                    <input type="text"  name="description" class="form-control" id="description" value="1" placeholder="pas dans la base de donnée" disabled>
                                </div>
                                <div class="form-row">
                                    <div class="input-group mb-3 col-sm-6">
                                        <div class="input-group-prepend">
                                            <label class="input-group-text">Début :</label>
                                        </div>
                                        <input type="date"  name="dateDebut" class="form-control" id="dateDebut"  placeholder="Date de début" disabled>
                                    </div>

                                    <div class="input-group mb-3 col-sm-6">
                                        <div class="input-group-prepend">
                                            <label class="input-group-text">Fin :</label>
                                        </div>
                                        <input type="date"  name="dateFin" class="form-control" id="dateFin"  placeholder="Date de fin" disabled>

                                    </div>
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <label class="input-group-text">Rémunération :</label>
                                    </div>
                                    <input type="text"  name="remuneration" class="form-control" id="remuneration" value="1" placeholder="Rémunération" disabled>

                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <label class="input-group-text">Lien web :</label>
                                    </div>
                                    <input type="text"  name="lienWeb" value="1" class="form-control" id="lienWeb"  placeholder="Lien web" disabled>

                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <label class="input-group-text">Lien Document :</label>
                                    </div>
                                    <input type="text"  name="document" value="1" class="form-control" id="document"  placeholder="Document" disabled>

                                </div>
                                <div id="criteresStageContent">

                                </div>



                                <form>
                                    <input type="hidden" name="tache" value="effectuerPostulationAction">
                                    <input type="hidden" id="idOffre" name="idOffre">
                                    <button type="submit" class="btn btn-block my-btn-outline-primary">Postuler</button>
                                </form>
                                </fieldset>
                            </div>

                        </div>
                    </div>
            </div>

            <!-- Fin footer -->
            <script>
                var nbComp = 0;


                var table = document.getElementById('table');

                for (var i = 1; i < table.rows.length; i++)
                {
                    table.rows[i].onclick = function ()
                    {
                        document.getElementById("nomCompagnie").value = this.cells[10].innerHTML;
                        document.getElementById("titre").value = this.cells[1].innerHTML;
                        document.getElementById("description").value = this.cells[2].innerHTML;
                        document.getElementById("dateDebut").value = this.cells[3].innerHTML;
                        document.getElementById("dateFin").value = this.cells[4].innerHTML;
                        document.getElementById("lienWeb").value = this.cells[5].innerHTML;
                        document.getElementById("nbVues").textContent = this.cells[7].innerHTML;
                        document.getElementById("fichier").value = this.cells[8].innerHTML;
                        document.getElementById("idOffre").value = this.cells[9].innerHTML;
                        document.getElementById("remuneration").value = this.cells[10].innerHTML;
                        document.getElementById("adresseCompagnie").value = this.cells[11].innerHTML;
                        document.getElementById("criteresStageContent").innerHTML = "";
                        $('#' + this.cells[9].innerHTML).children("td[name='crit']").each(function (index) {
                            document.getElementById("criteresStageContent").innerHTML +="<h2 class='Catpill'><span class='badge badge-pill mb-1'>"+$(this).text()+"</span></h2>";
                        });
                         $(".Catpill").addClass("blue")
                                 .css({"display": "inline"});


                        /*
                         console.log(document.getElementsByName("crit"+this.cells[9].innerHTML));
                         for (var x in document.getElementsByName("crit"+this.cells[9].innerHTML)){
                         document.getElementById("criteresStageContent").innerHTML += "<span class='badge badge-pill badge-primary'>" + x + "</span>";
                         }*/


                    }

                }
                ;

                $(document).ready(function () {
                    $(".elementCritere").click(function (event) {
                        document.getElementById("conteneurCompetences").innerHTML += "<span class='competence badge badge-primary'  onclick='enleverCompetence(" + nbComp + ")' id='competence" + nbComp + "'>" + event.target.innerHTML + " <a class='fas fa-times' ></a></span>";
                        $(".competence").addClass("alert alert-info ")
                                .css("margin", "2%");
                        nbComp++;
                    });

                });


            </script>
