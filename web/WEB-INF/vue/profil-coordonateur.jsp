
<%-- 
    Document   : profilEtudiant
    Created on : 2018-10-25, 10:17:32
    Author     : JP
--%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.List"%>
<%@page import="jdbc.Connexion"%>
<%@page import="jdbc.Config"%>
<%@page import="com.stageo.model.Utilisateur"%>
<%@page import="com.stageo.model.UtilisateurDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<jsp:useBean id="connexion" class="jdbc.Connexion"></jsp:useBean>
<jsp:useBean id="daoCritere" scope="page" class="com.stageo.model.CritereDAO">
    <jsp:setProperty name="daoCritere" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>
<jsp:useBean id="dao" class="com.stageo.model.UtilisateurDAO">
    <jsp:setProperty name="dao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>
<c:set var="listeCriteres" value="${daoCritere.findAll()}"/>
<c:set var="user" scope="page" value="${dao.read(sessionScope.connecte)}"/>

<div class="container" >
    <div class="col-6  mx-auto">
        <div class="card">
        <div class="card-header">
            <div class="card-title titreProfil"><h1> Espace coordonateur</h1>
            </div>
        </div>
        <div class="card-body">
            <div class="card-subtitle mb-3"><h4>Gestion des compétences:</h4>
            </div>


            <form action="*.do?tache=ajoutCritere" method = "post" >
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <label class="input-group-text">Nouvelle Compétence</label>
                        </div>
                        <input type="text"  name="critere" class="form-control" id="critere"  placeholder="Critère">
                        <button type="submit" class="btn my-btn-outline-primary my-2 my-sm-0"> Ajouter</button>
                    </div>

            </form>
            <div class="table-responsive">

                <table class="table table-striped">
                    <thead>
                        <tr>
                            <td >Critères <a href="#" class="fa fa-arrows-alt-v my-btn-outline-secondary"></a></td>
                            <td>Étudiants <a href="#" class="fa fa-arrows-alt-v my-btn-outline-secondary"></a></td>
                            <td>Requises <a href="#" class="fa fa-arrows-alt-v my-btn-outline-secondary"></a></td>
                            <td>Supprimer <a href="#" class="fa fa-arrows-alt-v my-btn-outline-secondary"></a></td>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${listeCriteres}" var="critere"> 
                            <tr>
                                <td>${critere.nom}</td>
                                <td>4</td>
                                <td>4</td>
                                <td>
                                    <form action="*.do?tache=supprimerCritere" method = "post">

                                        <input type="hidden" value="${critere.idCritere}" name="suppressionCrit" class="form-control" id="suppressionCrit">
                                        <button type="submit" class="my-btn-outline-primary"><a><i class="fas fa-trash-alt"></i></a></button>
                                    </form>
                                </td>
                            </tr>
                        </c:forEach> 

                    </tbody>
                </table>
            </div>
        </div>    
    </div> 
</div>
    </div>


