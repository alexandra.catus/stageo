
<!DOCTYPE html>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.List"%>
<%@page import="jdbc.Connexion"%>
<%@page import="jdbc.Config"%>
<%@page import="com.stageo.model.Utilisateur"%>
<%@page import="com.stageo.model.UtilisateurDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%/**
     * Connection cnx = Connexion.startConnection(Config.DB_USER, Config.DB_PWD,
     * Config.URL, Config.DRIVER); Utilisateur user; UtilisateurDAO userDao =
     * new UtilisateurDAO(cnx); Utilisateur userConnecte =
     * userDao.read((String)(session.getAttribute("connecte")));
     * if(session.getAttribute("connecte") != null) user = new
     * UtilisateurDAO(cnx).read((String)(session.getAttribute("connecte")));
     * else user = new
     * UtilisateurDAO(cnx).read((String)userConnecte.getIdUtilisateur());
     *
     * pageContext.setAttribute("user", user);
     *
     */

%>
<jsp:useBean id="connexion" class="jdbc.Connexion"></jsp:useBean>
<jsp:useBean id="dao" class="com.stageo.model.UtilisateurDAO">
    <jsp:setProperty name="dao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<jsp:useBean id="daoCompagnie" scope="page" class="com.stageo.model.CompagnieDAO">
    <jsp:setProperty name="daoCompagnie" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<c:set var="listeCompagnies" value="${daoCompagnie.findByIdEmployeur(sessionScope.connecte)}"/>
<c:set var="user" scope="page" value="${dao.read(sessionScope.connecte)}"/>

<c:set var="user" scope="page" value="${dao.read(sessionScope.connecte)}"/>

<div class="container" >



    <div class="row" >

        <div class="col-md-7">

            <div class="card">


                <div class="card-header titreProfil2">
                    <h1 class="card-title "> Ma compagnie</h1>
                </div>

                <div class="card-body titreProfil2">
                    <form class="form-horizontal" action="*.do?tache=effectuerCreationCompagnie" method="post">


                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <label class="input-group-text">Nom </label>
                            </div>
                            <input type="text"  name="nom" class="form-control" id="nom" "">
                        </div>
                        <fieldset> 
                            <legend><h5> Contact</h5></legend>
                            <div class="form-group ">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <label class="input-group-text">Site Web </label>
                                    </div>
                                    <input   name="siteWeb" class="form-control" id="siteWeb" >
                                </div>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <label class="input-group-text">Téléphone </label>
                                    </div>
                                    <input type="text"  name="tel" class="form-control" id="tel" >
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend><h5> Adresse </h5></legend>
                            <div class="form-group ">
                                <div class="form-row ">
                                    <div class="input-group mb-3 col-3">
                                        <div class="input-group-prepend">
                                            <label class="input-group-text">Bureau</label>
                                        </div>
                                        <input type="text"  name="bureau" class="form-control" id="bureau" >
                                    </div>

                                    <div class="input-group mb-3 col-3">
                                        <div class="input-group-prepend">
                                            <label class="input-group-text">No </label>
                                        </div>
                                        <input type="text"  name="noCivil" class="form-control" id="noCivil" >
                                    </div>
                                    <div class="input-group mb-3 col-6">
                                        <div class="input-group-prepend">
                                            <label class="input-group-text">Rue </label>
                                        </div>
                                        <input type="text"  name="rue" class="form-control" id="rue" >
                                    </div>
                                </div>



                                <div class="form-row ">
                                    <div class="input-group mb-3 col-6">
                                        <div class="input-group-prepend">
                                            <label class="input-group-text">Ville </label>
                                        </div>
                                        <input type="text"  name="ville" class="form-control" id="ville" >
                                    </div>
                                    <div class="input-group mb-3 col-6">
                                        <div class="input-group-prepend">
                                            <label class="input-group-text">Code postal </label>
                                        </div>
                                        <input type="text"  name="codePostal" class="form-control" id="codePostal" >
                                    </div>
                                </div>
                                <div class="form-row ">
                                    <div class="input-group mb-3 col-6">
                                        <div class="input-group-prepend">
                                            <label class="input-group-text">Province </label>
                                        </div>
                                        <input type="text"  name="province" class="form-control" id="province" >
                                    </div>
                                    <div class="input-group mb-3 col-6">
                                        <div class="input-group-prepend">
                                            <label class="input-group-text">Pays </label>
                                        </div>
                                        <input type="text"  name="pays" class="form-control" id="pays" >
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <input type="hidden" name="tache" value="effectuerModificationCompagnie">
                        <button type="submit" class="btn my-btn-outline-primary btnAccepte" style="width: 100%;">Enregister</button>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-5">

            <div class="card">

                <div class="card-header titreProfil">
                    <h1 class="card-title "> Mes informations</h1>
                </div>
                
                <div class="card-body">
                <form class="form-horizontal">
                    <div class="form-row">
                        <div class="form-group ">
                            <fieldset>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <label class="input-group-text">Nom :</label>
                                    </div>
                                    <input type="text"  name="nom" class="form-control" id="nom" value="${user.nom}" placeholder="${session.getAttribute("connecte")}">
                                </div> 
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <label class="input-group-text">Prénom :</label>
                                    </div>
                                    <input type="text"  name="prenom" class="form-control" id="prenom" value="${user.prenom}" placeholder="${user.prenom}">
                                </div>

                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <label class="input-group-text">Courriel :</label>
                                    </div>
                                    <input type="email" name="email" class="form-control" id="email" value="${user.email}" placeholder="${user.email}" >
                                </div>
                                <input type="hidden" name="tache" value="effectuerModificationUtilisateur">
                                <button type="submit" class="btn btn-block my-btn-outline-primary" name="modifie" >Enregistrer les modifications</button>
                            </fieldset>  
                        </div>

                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
                                </div>
