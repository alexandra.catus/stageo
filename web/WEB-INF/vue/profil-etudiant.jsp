
<%-- 
    Document   : profilEtudiant
    Created on : 2018-10-25, 10:17:32
    Author     : JP
--%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.List"%>
<%@page import="jdbc.Connexion"%>
<%@page import="jdbc.Config"%>
<%@page import="com.stageo.model.Utilisateur"%>
<%@page import="com.stageo.model.UtilisateurDAO"%>
<%@page import="com.stageo.model.Critere"%>
<%@page import="com.stageo.model.CritereDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<jsp:useBean id="daoCritere" scope="page" class="com.stageo.model.CritereDAO">
    <jsp:setProperty name="daoCritere" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>
<jsp:useBean id="dao" class="com.stageo.model.UtilisateurDAO">
    <jsp:setProperty name="dao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>
<c:set var="user" scope="page" value="${dao.read(sessionScope.connecte)}"/>
<c:set  var="listeCriteres" value="${daoCritere.findAll()}"/>



<div class="container" >
    <form class="form-horizontal">
        <div class="row" >
            <div class="col-md-5 mx-auto"  >
                <div class="card mb-3">
                    <div class="card-header">
                        <div class="card-title titreProfil"><h1>Mon profil <a href="#" class="btn my-btn-outline-primary" id="info">
                                    <i class="fas fa-info-circle"></i>
                                </a></h1></div>
                        <input type="hidden" name="tache" value="effectuerModificationUtilisateur">
                    </div>
                    <div class="mx-auto mb-3 mt-3">
                        <button type="submit" class="btn my-btn-outline-secondary mx-auto" name="modifie" >Enregistrer les modifications</button>
                    </div>

                </div>
            </div>
        </div>
        <div class="row" >

            <div class="col-md-5">
                <div class="card mb-3">

                    <div class="card-header titreProfil2">
                        <h1 class="card-title "> Coordonnées</h1>
                    </div>
                    <div class="card-body">

                        <div class="form-row">
                            <div class="form-group">
                                <fieldset>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <label class="input-group-text">Nom :</label>
                                        </div>
                                        <input type="text"  name="nom" class="form-control" id="nom" value="${user.nom}" placeholder="${session.getAttribute("connecte")}">
                                    </div>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <label class="input-group-text">Prénom :</label>
                                        </div>
                                        <input type="text"  name="prenom" class="form-control" id="prenom" value="${user.prenom}" placeholder="${user.prenom}">
                                    </div>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <label class="input-group-text">Courriel :</label>
                                        </div>
                                        <input type="email" name="email" class="form-control" id="email" value="${user.email}" placeholder="${user.email}" >
                                    </div>

                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <label class="input-group-text" >Téléphone :</label>
                                        </div>
                                        <input type="text"  placeholder="(514)980-2408" name="tel" disabled class="form-control" id="tel">
                                    </div>
                                </fieldset>
                                <fieldset>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-7">
                <div class="card mb-3">

                    <div class="card-header titreProfil2">
                        <h1 class="card-title "> Références</h1>
                    </div>
                    <div class="card-body">

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">CV</span>
                            </div>
                            <div class="custom-file">
                                <input type="file" class="form-control-file" accept=".pdf"  id="cv">

                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Relevé de note</span>
                            </div>
                            <div class="custom-file">
                                <input type="file"class="form-control-file" accept=".pdf"  id="relNote">
                            </div>
                        </div>


                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <label class="input-group-text">https://fr.linkedin.com/in/</label>
                            </div>
                            <input type="text"  class="form-control" id="lK" >
                        </div>

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <label class="input-group-text">https://gitlab.com/</label>
                            </div>
                            <input type="text" class="form-control" id="git" >

                        </div>

                        </fieldset>
                    </div>
                </div>

                <div class="card mb-3">


                    <div class="card-header titreProfil2">
                        <h1 class="card-title "> Profil de stagiaire </h1>
                    </div>

                    <div class="card-body">
                        <div class="form-group ">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend ">
                                    <label class="input-group-text " >Ma Recherche : </label>
                                </div>

                                <select   name="statutRecherche" class="form-control" >
                                    <c:if test="${user.getStatutRecherche() == 'en cours'}">
                                        <option name='en cours' value='en cours'>En cours</option>                  
                                        <option name='trouve' value='trouve'>J'ai trouvé</option>
                                    </c:if>
                                    <c:if test="${user.getStatutRecherche() == 'trouve'}">
                                        <option name='trouve' value='trouve'>J'ai trouvé</option>
                                        <option name='en cours' value='en cours'>En cours</option>                  

                                    </c:if>
                                </select>
                            </div>

                            <div class="input-group mb-3">
                                <div class="input-group-prepend ">
                                    <label class="input-group-text " >Domaine :</label>
                                </div>
                                <select name="specialite" class="form-control" >
                                    <option>Réseaux</option>
                                    <option>Programmation</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <table  id="competence" class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th scope="col">Critère <a href="#" class="fa fa-arrows-alt-v"></a></th>
                                            <th scope="col">Maitrîse <a href="#" class="fa fa-arrows-alt-v"></a></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <c:forEach items="${listeCriteres}" var="critere"> 
                                            <tr>
                                                <td name="critereNom">${critere.nom}
                                                </td>
                                                <td>
                                                    <input name="${critere.idCritere}" class="checkbox form-control mr-auto" type="checkbox" id="${critere.idCritere}">
                                                </td>
                                            </tr>
                                        </c:forEach> 

                                    </tbody>
                                </table>
                            </div>
                            </fieldset>
                            <fieldset>


                            </fieldset>
                        </div>
                    </div>
                </div>

                </form>



            </div>
        </div>
</div>



<script>
    $('#info').popover({trigger: "hover", placement: "right", content: "Profil consulté : 104 fois"});

    $('document').ready(function () {


        $(".checkbox").each(function (index) {
            this.checked = (${user.getListeCritere()}.includes(Number(this.id)));
        });
    });

</script>
