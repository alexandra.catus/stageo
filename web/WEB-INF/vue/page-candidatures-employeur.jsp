<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.List"%>
<%@page import="jdbc.Connexion"%>
<%@page import="jdbc.Config"%>
<%@page import="com.stageo.model.Utilisateur"%>
<%@page import="com.stageo.model.UtilisateurDAO"%>
<%@page import="com.stageo.model.EtudiantDAO"%>
<jsp:useBean id="connexion" class="jdbc.Connexion"></jsp:useBean>
<jsp:useBean id="dao" class="com.stageo.model.UtilisateurDAO">
    <jsp:setProperty name="dao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>
<c:set var="listeUser" value="${dao.findAll()}"/>

<div class="container" >



    <div class="row" >

        <div class="col-md-7">
            <div class="card">
                <div class="card-header titreProfil2">
                    <h1 class="card-title ">Mes candidats</h1>
                </div>
                <!-- Section de recherche des stages -->
                <!-- Barre de recherche -->
                <div class="card-body">
                    <table id="table" class="table table-hover borderTable">
                        <thead>
                            <tr>
                                <th>Nom <a class="sortBy" href="#"><span class="glyphicon glyphicon-sort-by-alphabet"></span></a></th>
                                <th>prenom <a class="sortBy" href="#"><span class="glyphicon glyphicon-sort-by-alphabet"></span></a></th>
                                <th>Courriel <a class="sortBy" href="#"><span class="glyphicon glyphicon-sort-by-alphabet"></span></a></th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${listeUser}" var="user">
                                <c:if test = "${user.role == 'etudiant'}">
                                    <c:if test = "${user.statutRecherche ne 'Candidature acceptee'}">
                                        <tr>

                                            <td>${user.nom}</td>
                                            <td>${user.prenom}</td>
                                            <td hidden>${user.email}</td>
                                            <td hidden>${user.statutRecherche}</td>
                                            <td hidden>${user.listeCritere}</td>
                                        </tr>
                                    </c:if>
                                </c:if>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <!-- Fin de la section de recherche des stages -->
        <div class="col-md-5" >
            <div class='card'>
                <div class="card-header titreProfil">
                    <h1>Détails de l'étudiant</h1>
                </div>
                <fieldset class="card-body">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <label class="input-group-text">Nom :</label>
                        </div>
                        <input disabled type="text"  name="nom" class="form-control" id="nom" value="${user.nom}" placeholder="${session.getAttribute("connecte")}">
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <label class="input-group-text">Prénom :</label>
                        </div>
                        <input  disabled type="text"  name="prenom" class="form-control" id="prenom" value="${user.prenom}" placeholder="${user.prenom}">
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <label class="input-group-text">Courriel :</label>
                        </div>
                        <input disabled type="email" name="email" class="form-control" id="email" value="${user.email}" placeholder="${user.email}" >
                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <label class="input-group-text" >Téléphone :</label>
                        </div>
                        <input disabled type="text"  placeholder=" pas dans la database" name="tel" disabled class="form-control" id="tel">
                    </div>

                    <div class="input-group mb-3">

                        <input type="text" class="form-control" disabled>
                        <div class="input-group-append ">
                            <label  class="input-group-text btn btn-primary ml-auto ">
                                <span class="">
                                    CV ...
                                    <input disabled type="file" class="form-control-file" accept=".pdf" style="display: none;" id="relNote">
                                </span>
                            </label>
                        </div>


                    </div>

                    <div class="input-group mb-3">

                        <input type="text" class="form-control" disabled>
                        <div class="input-group-append ">
                            <label  class="input-group-text btn btn-primary ml-auto ">
                                <span class="">
                                    Relevé de note ...
                                    <input disabled type="file" class="form-control-file" accept=".pdf" style="display: none;" id="relNote">
                                </span>
                            </label>
                        </div>


                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <label class="input-group-text">https://fr.linkedin.com/in/</label>
                        </div>
                        <input disabled type="text" placeholder=" pas dans la database"  class="form-control" id="lK" >
                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <label class="input-group-text">https://gitlab.com/</label>
                        </div>
                        <input disabled type="text" placeholder=" pas dans la database"  class="form-control" id="git" >

                    </div>

                    <fieldset>
                        <legend><h4>Profil de stagiaire 
                            </h4>
                        </legend>

                        <div class="input-group mb-3">
                            <div class="input-group-prepend ">
                                <label class="input-group-text " >Domaine :</label>
                            </div>
                            <input disabled type="text" placeholder=" pas dans la database"  class="form-control" id="domaine" >

                        </div>

                        <div class="form-group" id="critereEtudiant">

                            <c:forEach items="${listeCriteres}" var="critere"> 

                                <td name="critereNom">${critere.nom}

                                </c:forEach> 

                        </div>
                    </fieldset>
                    <fieldset>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend ">
                                <label class="input-group-text " >Ma Recherche : </label>
                            </div>
                            <input disabled type="text" class="form-control" id="statutRecherche" >

                        </div>
                    </fieldset>
                    <input type="hidden" name="tache" value="accepterCandidature">
                    <button type="submit" class="btn btn-primary btnAccepte" style="width: 100%;">Accepter Candidature</button>
                    </form>

                </fieldset>
            </div>

        </div>
    </div>
</div>

<!-- Fin footer -->
<script>

    var table = document.getElementById('table');

    for (var i = 1; i < table.rows.length; i++)
    {
        table.rows[i].onclick = function ()
        {
            //rIndex = this.rowIndex;
            document.getElementById("prenom").value = this.cells[0].innerHTML;
            document.getElementById("nom").value = this.cells[1].innerHTML;
            document.getElementById("email").value = this.cells[2].innerHTML;
            document.getElementById("statutRecherche").value = this.cells[3].innerHTML;
        };
    }

</script>