<%-- 
    Document   : page-candidatures-etudiant
    Created on : 2018-12-07, 01:23:06
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>


<%@page import="com.stageo.model.*"%>
<jsp:useBean id="dao" class="com.stageo.model.UtilisateurDAO">
    <jsp:setProperty name="dao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<jsp:useBean id="cdao" class="com.stageo.model.CompagnieDAO">
    <jsp:setProperty name="cdao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<jsp:useBean id="odao" class="com.stageo.model.StageDAO">
    <jsp:setProperty name="odao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>

<jsp:useBean id="edao" class="com.stageo.model.EmployeurDAO">
    <jsp:setProperty name="edao" property="cnx" value="${connexion.connection}"></jsp:setProperty>
</jsp:useBean>


<c:set var="user" scope="page" value="${dao.read(sessionScope.connecte)}"/>
<div class="container">
    <div class="row">
          <div class="col-mb-5 mx-auto">
        <div class="card">
             <div class="card-header titreProfil">
            <h4>Liste de vos candidatures en cours</h4>
             </div>
            <div class="card-body">
            <table id="stage" class="table table-hover">
                <thead>
                    <tr>
                        <td>Compagnie <a href="#" class="fa fa-arrows-alt-v"></a></td>
                        <td>Poste <a href="#" class="fa fa-arrows-alt-v"></a></td>
                        <td>Date de contact <a href="#" class="fa fa-arrows-alt-v"></a></td>
                        <td>Statut <a href="#" class="fa fa-arrows-alt-v"></a></td>
                    </tr>
                </thead>
                <tbody>
                <c:forEach items="${user.liste_candidature}" var="candidature">
                    <fmt:formatDate var="date" value="${candidature.date}" pattern="yyyy-MM-dd" />
                    <c:set var="offre" value="${odao.read(candidature.idOffre)}"/>
                    <c:set var="employeur" value="${edao.read(offre.idEmployeur)}"/>
                    <c:set var="compagnieid" value="${employeur.compagnie.idCompagnie}"/>
                    <c:set var="compagnie" value="${cdao.read(compagnieid)}"/>
                    
                    <tr>
                        <td>${compagnie.nom}</td>
                        <td>${offre.titre}</td>
                        <td>${date}</td>
                        <td>${candidature.statut}</td>
                    </tr>
                    </c:forEach>
                </tbody>
            </table>
                </div>
        </div>
      </div>
          </div>
      </div>