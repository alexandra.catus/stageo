
<!--
    Author: Alex -- La gestion des erreurs dans le form est effectuée.
-->
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>

<div class="Accueil">
<div class="container " >
    <div id="carteinscription" class="card inscription">
        <div class="card-body">
            <h1 class="card-title inscriptionTitre">Nous rejoindre</h1>
            <h6 class="card-subtitle mb-2 text-muted"> Vous avez l'expertise</h6>
            <h6 class="card-subtitle mb-2 text-muted"> Nous avons les talents</h6>
            <form  id="formInscription" onSubmit="return validerFormInscription()" action="*.do?tache=effectuerInscription" method="post">
                <div class="row">
                        <div class="input-group mb-3 ml-auto col-sm-6">
                            <div class="input-group-prepend">
                                <label class="input-group-text">Nom</label>
                                <input type="text" class="form-control col-9" id="prenom" name="nom" required>
                            </div>
                        </div>
                        <div class="input-group mb-3 ml-auto col-sm-6">
                            <div class="input-group-prepend">
                                <label class="input-group-text">Prenom</label>
                            </div>
                            <input type="text" class="form-control" id="nom" name="prenom" required>
                        </div>
                </div>
                <div class="input-group mb-3 ml-auto">
                    <div class="input-group-prepend">
                        <label class="input-group-text">Courriel</label>
                    </div>
                    <input type="email" class="form-control" id="courriel" name="courriel" required>
                </div>
                <div class="input-group mb-3 ml-auto">
                    <div class="input-group-prepend">
                        <label class="input-group-text">Mot de Passe</label>
                    </div>
                    <input  type="password" class="form-control" id="motDePasse" name="motDePasse" required>
                </div>
                <div class="input-group mb-3 ml-auto">
                    <div class="input-group-prepend">
                        <label class="input-group-text"> Confirmer Mot de passe</label>
                    </div>
                    <input type="password" class="form-control" id="motDePasseRepeat" name="motDePasseRepeat"required>
                </div>

                <div class="input-group mb-3 ml-auto">
                    <div class="input-group-prepend">
                        <label class="input-group-text">Je suis </label>
                    </div>
                    <select class="form-control"  id="role" name="role">
                        <option value="etudiant"> étudiant <i class="fas fa-caret-down"></i></option>
                        <option value="employeur" > employeur</option>
                    </select>
                </div>

                <input type="hidden" name="tache" value="effectuerInscription">
                <button type="submit" class="btn btn-block my-btn-outline-secondary">S'inscrire</button>
            </form>
        </div>
    </div>
</div>
</div>
</div>
    </div>
