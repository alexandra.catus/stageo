/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.model;

import com.util.Util;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author admin
 */
public class CritereDAO extends DAO<Critere> {

    public CritereDAO() {
    }

    public CritereDAO(Connection cnx) {
        super(cnx);
    }

    @Override
    public boolean create(Critere x) {
        String req = "INSERT INTO critere (`ID_CRITERE` , `NOM`) VALUES (?, ?)";

        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);
            paramStm.setString(1, x.getIdCritere());
            paramStm.setString(2, x.getNom());

            int nbLignesAffectees = paramStm.executeUpdate();

            if (nbLignesAffectees > 0) {
                paramStm.close();
                return true;
            }

            return false;
        } catch (SQLException exp) {
        } finally {
            try {
                if (paramStm != null) {
                    paramStm.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(CritereDAO.class.getName())
                        .log(Level.SEVERE, null, ex);
            }

        }
        return false;
    }

    @Override
    public Critere read(int id) {
        String req = "SELECT * FROM critere WHERE `ID_Critere` = ?";

        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);

            paramStm.setInt(1, id);

            ResultSet resultat = paramStm.executeQuery();

            // On vérifie s'il y a un résultat    
            if (resultat.next()) {

                Critere n = new Critere();
                n.setIdCritere(resultat.getString("ID_CRITERE"));
                n.setNom(resultat.getString("NOM"));

                resultat.close();
                paramStm.close();
                return n;
            }

            resultat.close();
            paramStm.close();
            return null;

        } catch (SQLException exp) {
        } finally {
            try {
                if (paramStm != null) {
                    paramStm.close();
                }
            } catch (SQLException exp) {
            } catch (Exception e) {
            }
        }

        return null;
    }

    @Override
    public Critere read(String id) {

        try {
            return this.read(Integer.parseInt(id));
        } catch (NumberFormatException e) {
            return null;
        }

    }

    @Override
    public boolean update(Critere x) {
        String req = "UPDATE critere SET `NOM` = ? WHERE `ID_CRITERE` = ?";

        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);

            if (x.getIdCritere() == null || "".equals(x.getIdCritere().trim())) {
                paramStm.setString(1, null);
            } else {
                paramStm.setString(1, x.getNom());
            }

            int nbLignesAffectees = paramStm.executeUpdate();

            if (nbLignesAffectees > 0) {
                paramStm.close();
                return true;
            }

            return false;
        } catch (SQLException exp) {
        } finally {
            try {
                if (paramStm != null) {
                    paramStm.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(CritereDAO.class.getName())
                        .log(Level.SEVERE, null, ex);
            }

        }
        return false;
    }

    public boolean delete(String x) {
        String req = "DELETE FROM critere WHERE `ID_CRITERE` = ?";

        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);
            paramStm.setString(1, x);

            int nbLignesAffectees = paramStm.executeUpdate();

            if (nbLignesAffectees > 0) {
                paramStm.close();
                return true;
            }

            return false;
        } catch (SQLException exp) {
        } finally {
            try {
                if (paramStm != null) {
                    paramStm.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(CritereDAO.class.getName())
                        .log(Level.SEVERE, null, ex);
            }

        }
        return false;
    }

    @Override
    public List<Critere> findAll() {
        List<Critere> liste = new LinkedList<>();
        try {
            Statement stm = cnx.createStatement();
            ResultSet r = stm.executeQuery("SELECT * FROM critere");
            while (r.next()) {
                Critere n = new Critere();
                n.setIdCritere(r.getString("ID_CRITERE"));
                n.setNom(r.getString("NOM"));

                liste.add(n);
            }
            // Collections.sort(liste);
            Collections.reverse(liste);
            r.close();
            stm.close();
        } catch (SQLException exp) {
        }
        return liste;
    }

    public Critere findById(String id) {
        String req = "SELECT * FROM critere WHERE `ID_critere` = ?";

        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);
            paramStm.setString(1, id);

            ResultSet resultat = paramStm.executeQuery();

            // On vérifie s'il y a un résultat    
            if (resultat.next()) {

                Critere n = new Critere();
                n.setIdCritere(resultat.getString("ID_CRITERE"));
                n.setNom(resultat.getString("NOM"));

                resultat.close();
                paramStm.close();
                return n;
            }

            resultat.close();
            paramStm.close();
            return null;

        } catch (SQLException exp) {
        } finally {
            try {
                if (paramStm != null) {
                    paramStm.close();
                }
            } catch (SQLException exp) {
            } catch (Exception e) {
            }
        }

        return null;
    }

    public List<Critere> findByOffre(String id) {
        List<Critere> liste = new LinkedList<>();
        String req = "SELECT * FROM offrestagecritere WHERE `ID_OFFRE` = ?";
        String req2 = "SELECT * FROM critere WHERE `ID_CRITERE` = ?";
        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);
            paramStm.setString(1, id);

            ResultSet resultat = paramStm.executeQuery();

            // On vérifie s'il y a un résultat    
            while(resultat.next()) {
                Critere n = new Critere();
                n.setIdCritere(resultat.getString("ID_CRITERE"));
                paramStm=cnx.prepareStatement(req2);
                paramStm.setString(1, n.getIdCritere());
                ResultSet resultat2 = paramStm.executeQuery();
                if (resultat2.next()) {
                    System.out.println("Resultat 4: " + resultat2);
                    n.setNom(resultat2.getString("NOM"));
                }
                liste.add(n);
                
            }
            resultat.close();
                paramStm.close();
                return liste;

        } catch (SQLException exp) {
        } finally {
            try {
                if (paramStm != null) {
                    paramStm.close();
                }
            } catch (SQLException exp) {
            } catch (Exception e) {
            }
        }

        return null;
    }

    public Critere findNomByOffre(String id) {
        String req = "SELECT * FROM offrestagecritere WHERE `ID_OFFRE` = ?";

        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);
            paramStm.setString(1, id);

            ResultSet resultat = paramStm.executeQuery();

            System.out.println("Resultat1: " + resultat);
            // On vérifie s'il y a un résultat    
            if (resultat.next()) {
                System.out.println("Resultat2: " + resultat);
                Critere n = new Critere();
                System.out.println("Resultat3: " + resultat.getString("ID_CRITERE"));
                n.setIdCritere(resultat.getString("ID_CRITERE"));
                System.out.println("Resultat4: " + resultat.getString("NOM"));
                n.setNom(resultat.getString("NOM"));
                System.out.println("Resultat5: " + resultat);
                resultat.close();
                paramStm.close();
                return n;
            }

            resultat.close();
            paramStm.close();
            return null;

        } catch (SQLException exp) {
        } finally {
            try {
                if (paramStm != null) {
                    paramStm.close();
                }
            } catch (SQLException exp) {
            } catch (Exception e) {
            }
        }

        return null;
    }

    @Override
    public boolean delete(Critere x) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
