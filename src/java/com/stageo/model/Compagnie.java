/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.model;



import java.util.UUID;
/**
 *
 * @author JP
 */
public class Compagnie/* implements Comparable<Compagnie>*/{

    private Adresse adresse;
    private String idCompagnie, //clé primaire
                nom = "nom de la compagnie",
                pageWeb = "http://www.exemple.com";



    //Constructeurs
    public Compagnie() {
        String uniqueID = UUID.randomUUID().toString();
        this.adresse = new Adresse();
        this.idCompagnie = uniqueID;
    }

    public Compagnie(String idCompagnie, String nom, String pageWeb, Adresse adresse) {
        this.idCompagnie = idCompagnie;
        this.nom = nom;
        this.pageWeb = pageWeb;
        this.adresse = adresse;
    }
     public Compagnie(String nom, String pageWeb, Adresse adresse) {

        String uniqueID = UUID.randomUUID().toString();

        this.idCompagnie = uniqueID;
        this.nom = nom;
        this.pageWeb = pageWeb;
        this.adresse = adresse;
    }
        public Compagnie(String idCompagnie, String nom, String pageWeb) {
        this.idCompagnie = idCompagnie;
        this.nom = nom;
        this.pageWeb = pageWeb;
        this.adresse = new Adresse();
    }

    // Getters et Setters
    public String getIdCompagnie() {
        return idCompagnie;
    }

    public void setIdCompagnie(String idCompagnie) {
        this.idCompagnie = idCompagnie;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPageWeb() {
        return pageWeb;
    }

    public void setPageWeb(String pageWeb) {
        this.pageWeb = pageWeb;
    }

    public Adresse getAdresse() {
        return adresse;
    }

    public void setAdresse(Adresse adresse) {
        this.adresse = adresse;
    }

  /*@Override
    public int compareTo(Compagnie Compagnie) {
        int valeur = 0;
        if(this.point > Compagnie.point)
            valeur = 1;
        else if(this.point < Compagnie.point)
            valeur = -1;
        return valeur;
    }*/


    @Override
    public boolean equals(Object obj) {
        if(this != null && obj != null)
            if(obj instanceof Compagnie)
                return (this.idCompagnie == ((Compagnie)obj).getIdCompagnie());


        return false;

    }
}
