/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.model;

import com.util.Util;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Chris
 */
public class MessageDAO extends DAO<Message> {
    
    public MessageDAO() {
    }
    
    public MessageDAO(Connection cnx) {
        super(cnx);
    }
    
    @Override
    public boolean create(Message x) {
        String req = "INSERT INTO message (`TITRE`,`MESSAGE`,`VU`,`DATE`,`HEURE`,`ID_EXPEDITEUR`) VALUES (?,?,?,?,?,?)";

        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);
            paramStm.setString(1, Util.toUTF8(x.getTitre()));
            paramStm.setString(2, Util.toUTF8(x.getMessage()));
            paramStm.setBoolean(3, x.getVu());     
            paramStm.setString(4, Util.toUTF8(x.getDate()));
            paramStm.setString(5, Util.toUTF8(x.getHeure()));
            paramStm.setString(6, Util.toUTF8(x.getIdExpediteur()));
                 

            int nbLignesAffectees= paramStm.executeUpdate();

            if (nbLignesAffectees>0) {
                    paramStm.close();
                    return true;
            }
                
            return false;
        }
        catch (SQLException exp) {
        }
        finally {
                try {
                    if (paramStm!=null)
                        paramStm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(MessageDAO.class.getName())
                            .log(Level.SEVERE, null, ex);
                }
                
        }
        return false;
    }
    @Override
    public Message read(int id) {
        String req = "SELECT * FROM message WHERE `ID_MESSAGE` = ?";
        
        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);

            paramStm.setInt(1, id);

            ResultSet r = paramStm.executeQuery();

            // On vérifie s'il y a un résultat    
            if(r.next()){

                Message m = new Message();
                m.setIdMessage(r.getString("ID_MESSAGE"));
                m.setTitre(r.getString("TITRE"));
                m.setMessage(r.getString("MESSAGE"));
                m.setVu(r.getBoolean("VU"));
                m.setDate(r.getString("DATE"));
                m.setHeure(r.getString("HEURE"));
                m.setIdExpediteur(r.getString("ID_EXPEDITEUR"));
             
                
                r.close();
                paramStm.close();
                    return m;
            }
            
            r.close();
            paramStm.close();
            return null;
                
        }
        catch (SQLException exp) {
        }
        finally {
            try{
                if (paramStm!=null)
                    paramStm.close();
            }
            catch (SQLException exp) {
            }
             catch (Exception e) {
            }
        }        
        
        return null;
    }

    @Override
    public Message read(String id) {
        
        try{
            return this.read(Integer.parseInt(id));
        }
        catch(NumberFormatException e){
            return null;
        }

    }

    @Override
    public boolean update(Message x) { 
        String req = "UPDATE message SET `TITRE` = ?,`MESSAGE` = ?, `VU` = ?,`ID_EXPEDITEUR`= ?,`DATE` = ?,`HEURE` = ? WHERE `ID_MESSAGE` = ?";

        PreparedStatement paramStm = null;
        try {

                paramStm = cnx.prepareStatement(req);

                if(x.getIdMessage() == null || "".equals(x.getIdMessage().trim()))
                    paramStm.setString(1, null);
                else
                    paramStm.setString(1, x.getTitre());
                    paramStm.setString(2, x.getMessage());
                    paramStm.setBoolean(3, x.getVu());
                    paramStm.setString(4, x.getDate());
                    paramStm.setString(5, x.getHeure());
                    paramStm.setString(6, x.getIdExpediteur());

                int nbLignesAffectees= paramStm.executeUpdate();
                
                if (nbLignesAffectees>0) {
                        paramStm.close();
                        return true;
                }
                
            return false;
        }
        catch (SQLException exp) {
        }
        finally {
                try {
                    if (paramStm!=null)
                        paramStm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(MessageDAO.class.getName())
                            .log(Level.SEVERE, null, ex);
                }
                
        }
        return false;
    }

    @Override
    public boolean delete(Message x) {
        String req = "DELETE FROM message WHERE `ID_MESSAGE` = ?";

        PreparedStatement paramStm = null;
        try {

                paramStm = cnx.prepareStatement(req);
                paramStm.setString(1, x.getIdMessage());
                
                int nbLignesAffectees= paramStm.executeUpdate();
                
                if (nbLignesAffectees>0) {
                        paramStm.close();
                    return true;
                }
                
            return false;
        }
        catch (SQLException exp) {
        }
        finally {
                try {
                    if (paramStm!=null)
                        paramStm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(MessageDAO.class.getName())
                            .log(Level.SEVERE, null, ex);
                }
                
        }
        return false;
    }

   
    @Override 
    public List<Message> findAll() {
        List<Message> liste = new LinkedList<>();
        try {
            Statement stm = cnx.createStatement(); 
            ResultSet r = stm.executeQuery("SELECT * FROM message");
            while (r.next()) {
                Message m = new Message();
                m.setIdMessage(r.getString("ID_MESSAGE"));
                m.setTitre(r.getString("TITRE"));
                m.setMessage(r.getString("MESSAGE"));
                m.setVu(r.getBoolean("VU"));
                m.setDate(r.getString("DATE"));
                m.setHeure(r.getString("HEURE"));
                m.setIdExpediteur(r.getString("ID_EXPEDITEUR"));
                           			
                liste.add(m);
            }
           // Collections.sort(liste);
            Collections.reverse(liste);
            r.close();
            stm.close();
        }
        catch (SQLException exp) {
        }
        return liste;
    } 

    public Message findById(String id) {
        String req = "SELECT * FROM message WHERE `ID_MESSAGE` = ?";
        
        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);      
            paramStm.setString(1, Util.toUTF8(id));

            ResultSet resultat = paramStm.executeQuery();

            // On vérifie s'il y a un résultat    
            if(resultat.next()){

                Message m = new Message();
                m.setIdMessage(resultat.getString("ID_MESSAGE"));
                m.setTitre(resultat.getString("TITRE"));
                m.setMessage(resultat.getString("MESSAGE"));
                m.setVu(resultat.getBoolean("VU"));
                m.setDate(resultat.getString("DATE"));
		m.setHeure(resultat.getString("HEURE"));
                m.setIdExpediteur(resultat.getString("ID_EXPEDITEUR"));

                resultat.close();
                paramStm.close();
                    return m;
            }
            
            resultat.close();
            paramStm.close();
            return null;
                
        }
        catch (SQLException exp) {
        }
        finally {
            try{
                if (paramStm!=null)
                    paramStm.close();
            }
            catch (SQLException exp) {
            }
             catch (Exception e) {
            }
        }        
        
        return null;
    }
    
}
