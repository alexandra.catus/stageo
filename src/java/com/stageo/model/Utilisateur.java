/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.model;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author Dave
 */

public class Utilisateur {
       

    // Attributs propres à un utilisateur
    private String idUtilisateur,                  // clé primaire
                   email,
                   motDePasse,
                   nom,
                   prenom,
                   role;
    
    private List<OffreStage> liste_offreStage;
    
    // Attributs propres à un étudiant
    private CV cv;
    private String statutRecherche = "en cours";
    private List<Critere> listeCritere;
    private List<Candidature> liste_candidature;
    
    // Attributs propres à un employeur
    private Compagnie compagnie;
    private String telephone;
    
    // Attributs propres à un coordonnateur
    private List<Document> liste_document;
    private List<Notification> liste_notification;
    
    
    // Constructeur sans argument
    public Utilisateur(){
        String uniqueID = UUID.randomUUID().toString();
        
        this.idUtilisateur = uniqueID;
        this.liste_candidature = new LinkedList<Candidature>();
        this.liste_offreStage = new LinkedList<OffreStage>();
        this.listeCritere = new LinkedList<Critere>();
        this.compagnie = new Compagnie();
        this.liste_document = new LinkedList<Document>();
        this.liste_notification = new LinkedList<Notification>();
    }
    
    // Constructeur AVEC ID
    public Utilisateur( String idUtilisateur,
                        String email,
                        String motDePasse,
                        String nom,
                        String prenom,
                        String role){
        this.idUtilisateur = idUtilisateur;
        this.email = email;
        this.motDePasse = motDePasse;
        this.nom = nom;
        this.prenom = prenom;
        this.role = role;
        this.liste_candidature = new LinkedList<Candidature>();
        this.liste_offreStage = new LinkedList<OffreStage>();
        this.listeCritere = new LinkedList<Critere>();
        this.compagnie = new Compagnie();
        this.liste_document = new LinkedList<Document>();
        this.liste_notification = new LinkedList<Notification>();
        
    }
    
    // Constructeur SANS ID
    public Utilisateur( String email,
                        String motDePasse,
                        String nom,
                        String prenom,
                        String role){
        
        String uniqueID = UUID.randomUUID().toString();
        
        this.idUtilisateur = uniqueID;
        this.email = email;
        this.motDePasse = motDePasse;
        this.nom = nom;
        this.prenom = prenom;
        this.role = role;
        this.liste_candidature = new LinkedList<Candidature>();
        this.liste_offreStage = new LinkedList<OffreStage>();
        this.listeCritere = new LinkedList<Critere>();
        this.compagnie = new Compagnie();
        this.liste_document = new LinkedList<Document>();
        this.liste_notification = new LinkedList<Notification>();
    }

   public String getIdUtilisateur() {
        return idUtilisateur;
    }

    public void setIdUtilisateur(String idUtilisateur) {
        this.idUtilisateur = idUtilisateur;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public List<OffreStage> getListe_offreStage() {
        return liste_offreStage;
    }

    public void setListe_offreStage(List<OffreStage> liste_offreStage) {
        this.liste_offreStage = liste_offreStage;
    }

    public List<Candidature> getListe_candidature() {
        return liste_candidature;
    }

    public void setListe_candidature(List<Candidature> liste_candidature) {
        this.liste_candidature = liste_candidature;
    }

    public CV getCv() {
        return cv;
    }

    public void setCv(CV cv) {
        this.cv = cv;
    }

    public String getStatutRecherche() {
        return statutRecherche;
    }

    public void setStatutRecherche(String statutRecherche) {
        this.statutRecherche = statutRecherche;
    }

    public List<Critere> getListeCritere() {
        return listeCritere;
    }

    public void setListeCritere(List<Critere> liste_critere) {
        this.listeCritere = liste_critere;
    }

    public Compagnie getCompagnie() {
        return compagnie;
    }

    public void setCompagnie(Compagnie compagnie) {
        this.compagnie = compagnie;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public List<Document> getListe_document() {
        return liste_document;
    }

    public void setListe_document(List<Document> liste_document) {
        this.liste_document = liste_document;
    }

    public List<Notification> getListe_notification() {
        return liste_notification;
    }

    public void setListe_notification(List<Notification> liste_notification) {
        this.liste_notification = liste_notification;
    }
    
    
    
}
