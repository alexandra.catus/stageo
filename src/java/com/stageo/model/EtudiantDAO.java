/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.model;

import com.util.Util;

/**
 *
 * @author Dave
 */
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EtudiantDAO extends DAO<Utilisateur>{

    public EtudiantDAO() {
    }

    public EtudiantDAO(Connection cnx) {
        super(cnx);
    }

    @Override
    public boolean create(Utilisateur x) {
        String req = "INSERT INTO etudiant (`ID_ETUDIANT`, `STATUT_RECHERCHE`) VALUES (?,?)";

        PreparedStatement paramStm = null;
        try {

                paramStm = cnx.prepareStatement(req);

              if(x.getIdUtilisateur() != null && !"".equals(x.getIdUtilisateur().trim())){
                paramStm.setString(1, Util.toUTF8(x.getIdUtilisateur()));
                if(x.getStatutRecherche() != null && !"".equals(x.getStatutRecherche().trim()))
                  paramStm.setString(2, Util.toUTF8(x.getStatutRecherche()));
                else
                    paramStm.setString(2, "en cours");

                int nbLignesAffectees= paramStm.executeUpdate();

                if (nbLignesAffectees>0) {
                        paramStm.close();
                        return true;
                }
            }
            return false;
        }
        catch (SQLException exp) {
        }
        finally {
                try {
                    if (paramStm!=null)
                        paramStm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(EtudiantDAO.class.getName())
                            .log(Level.SEVERE, null, ex);
                }
        }
        return false;
    }

    @Override
    public Utilisateur read(String id) {
        String req = "SELECT * FROM etudiant WHERE `ID_ETUDIANT` = ?";
        String req2 = "SELECT * FROM etudiantcritere WHERE `ID_ETUDIANT` = ?";
        String req3 = "SELECT * FROM candidature WHERE `ID_ETUDIANT` = ?";
        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);

            paramStm.setString(1, id);

            ResultSet resultat = paramStm.executeQuery();

            // On vérifie s'il y a un résultat
            if(resultat.next()){

                Utilisateur etudiant = new Utilisateur();
                
               
                etudiant.setIdUtilisateur(resultat.getString("ID_ETUDIANT"));
                if((etudiant.getStatutRecherche() != null) && (!"".equals(etudiant.getStatutRecherche().trim())))
                    etudiant.setStatutRecherche(resultat.getString("STATUT_RECHERCHE"));
                else
                    etudiant.setStatutRecherche("en cours");
                
            paramStm = cnx.prepareStatement(req2);
            paramStm.setString(1, id);
            paramStm = cnx.prepareStatement(req3);
            paramStm.setString(1, id);
             List<Candidature> listeCandidature= new ArrayList<Candidature>();
            resultat = paramStm.executeQuery();
            Candidature c;
            while (resultat.next()) {
                c= new Candidature(resultat.getString("ID_OFFRE"),resultat.getTimestamp("DATE"), resultat.getString("STATUT"));
                listeCandidature.add(c);
                
            }
            etudiant.setListe_candidature(listeCandidature);

                resultat.close();
                paramStm.close();
                 return etudiant;
                    
            }

            resultat.close();
            paramStm.close();
            return null;

        }
        catch (SQLException exp) {
            System.out.println("Erreur 1 SQL (classe etudiantDAO) :"+exp);
        }
        finally {
            try{
                if (paramStm!=null)
                    paramStm.close();
            }
            catch (SQLException exp) {
                System.out.println("Erreur 2 SQL (classe etudiantDAO) :"+exp);
            }
             catch (Exception e) {
                 System.out.println("Erreur 3 (classe etudiantDAO) :"+e);
            }
        }

        return null;

    }
    @Override
    public boolean update(Utilisateur x) {
        
        String id=x.getIdUtilisateur();
        String req1 = "UPDATE etudiant SET `STATUT_RECHERCHE` = ? WHERE `ID_ETUDIANT` = ?";
        String req2 = "DELETE FROM etudiantCritere WHERE `ID_ETUDIANT` = ?";
        String req3 = "INSERT INTO etudiantCritere (`ID_ETUDIANT`, `ID_CRITERE`) VALUES (?,?)";
        PreparedStatement paramStm;
        try {
            paramStm = cnx.prepareStatement(req1);
            paramStm.setString(1, x.getStatutRecherche());
            paramStm.setString(2,id);
            int nbLignesAffecteesUpdate= paramStm.executeUpdate();
            paramStm = cnx.prepareStatement(req2);
            paramStm.setString(1, id);
            int nbLignesAffecteesDelete= paramStm.executeUpdate();
            int nbLignesCriteresAffecteesUpdate=0;
            for (int i = 0; i < x.getListeCritere().size(); i++){
                paramStm = cnx.prepareStatement(req3);
                paramStm.setString(1, id);
                paramStm.setString(2, x.getListeCritere().get(i).getIdCritere());
                nbLignesCriteresAffecteesUpdate = paramStm.executeUpdate();
            }
            paramStm.close();
            if (nbLignesAffecteesUpdate>0&&nbLignesCriteresAffecteesUpdate>0){
                 
            return true;
            }
            }
            
        catch (SQLException exp) {
           Logger.getLogger(EtudiantDAO.class.getName())
                            .log(Level.SEVERE, null, exp);
        }
        return false;
    }

    @Override
    public boolean delete(Utilisateur x) {
        String req = "DELETE FROM etudiant WHERE `ID_ETUDIANT` = ?";

        PreparedStatement paramStm = null;

        try {
                paramStm = cnx.prepareStatement(req);
                paramStm.setString(1, x.getIdUtilisateur());

                int nbLignesAffectees= paramStm.executeUpdate();

                if (nbLignesAffectees>0) {
                    paramStm.close();
                    return true;
                }

            return false;
        }
        catch (SQLException exp) {
        }
        catch (Exception exp) {
        }
        finally {
                try {
                    if (paramStm!=null)
                        paramStm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(EtudiantDAO.class.getName())
                            .log(Level.SEVERE, null, ex);
                }
        }
        return false;
    }

    @Override
    public List<Utilisateur> findAll() {

        List<Utilisateur> liste = new LinkedList<>();
        try {
            Statement stm = cnx.createStatement();
            ResultSet r = stm.executeQuery("SELECT * FROM etudiant");
            while (r.next()) {
                Utilisateur etudiant = new Utilisateur();
                etudiant.setIdUtilisateur(r.getString("ID_ETUDIANT"));
                etudiant.setStatutRecherche(r.getString("STATUT_RECHERCHE"));

                liste.add(etudiant);
            }
            r.close();
            stm.close();
        }
        catch (SQLException exp){
        }
        return liste;

    }

    public Utilisateur findByStatutRecherche(String statutRecherche) {
        String req = "SELECT * FROM etudiant WHERE `STATUT_RECHERCHE` = ?";

        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);

            paramStm.setString(1, Util.toUTF8(statutRecherche));

            ResultSet resultat = paramStm.executeQuery();

            // On vérifie s'il y a un résultat
            if(resultat.next()){

                Utilisateur etudiant = new Utilisateur();
                etudiant.setIdUtilisateur(resultat.getString("ID_ETUDIANT"));
                etudiant.setStatutRecherche(resultat.getString("STATUT_RECHERCHEw"));

                resultat.close();
                paramStm.close();
                    return etudiant;
            }

            resultat.close();
            paramStm.close();
            return null;
        }
        catch (SQLException exp) {}
        finally {
            try{
                if (paramStm!=null)
                    paramStm.close();
            }
            catch (SQLException exp) {}
        }
        return null;
    }

    @Override
    public Utilisateur read(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
