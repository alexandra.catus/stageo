/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.model;

import java.io.InputStream;
import java.sql.Timestamp;
import java.util.UUID;

/**
 *
 * @author Christopher
 * 
 */
public class Document {
    
    private String idDocument, titre, type, idCoordonnateur;
    private Timestamp date;
    private int nbVues;
    private InputStream fichier;
    
    public Document() {
    }
    public Document (String titre, String type, String idCoordonnateur, InputStream fichier){
        String uniqueID = UUID.randomUUID().toString();    
        this.idDocument = uniqueID;
        this.titre = titre;
        this.type = type;
        this.idCoordonnateur = idCoordonnateur;
        this.nbVues = 0;
        this.date = new Timestamp(System.currentTimeMillis());
        this.fichier = fichier;
    }
    public Document (String idDocument, String titre, String type, int nbVues, String idCoordonnateur, Timestamp date){    
        this.idDocument = idDocument;
        this.titre = titre;
        this.type = type;
        this.nbVues = nbVues;
        this.idCoordonnateur = idCoordonnateur;
        this.date = date;
    }

    public String getIdDocument() {
        return idDocument;
    }

    public void setIdDocument(String idDocument) {
        this.idDocument = idDocument;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIdCoordonnateur() {
        return idCoordonnateur;
    }

    public void setIdCoordonnateur(String idCoordonnateur) {
        this.idCoordonnateur = idCoordonnateur;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public int getNbVues() {
        return nbVues;
    }

    public void setNbVues(int nbVues) {
        this.nbVues = nbVues;
    }

    public InputStream getFichier() {
        return fichier;
    }

    public void setFichier(InputStream fichier) {
        this.fichier = fichier;
    }
    
}