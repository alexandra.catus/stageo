/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.model;

import com.util.Util;
/**
 *
 * @author Dave
 */

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dave
 * @author Alex : @Todo: il faut repasser sur les return, pas urgent
 */

public class UtilisateurDAO extends DAO<Utilisateur>{

    public UtilisateurDAO() {
    }

    public UtilisateurDAO(Connection cnx) {
        super(cnx);
    }

    @Override
    public boolean create(Utilisateur x) {
        String req = "INSERT INTO utilisateur (`ID_UTILISATEUR`, `COURRIEL`, `MOT_DE_PASSE`, `NOM`, "
                + "`PRENOM`, `TYPE_UTILISATEUR`) VALUES (?,?,?,?,?,?)";

        PreparedStatement paramStm = null;
        boolean success;
        try {
                paramStm = cnx.prepareStatement(req);

              if(x.getIdUtilisateur() != null && !"".equals(x.getIdUtilisateur().trim())
              && x.getEmail() != null && !"".equals(x.getEmail().trim())
              && x.getMotDePasse() != null && !"".equals(x.getMotDePasse().trim())
              && x.getNom()      != null && !"".equals(x.getNom().trim())
              && x.getPrenom()   != null && !"".equals(x.getPrenom().trim())){
                paramStm.setString(1, x.getIdUtilisateur());
                paramStm.setString(2, x.getEmail());
                paramStm.setString(3, x.getMotDePasse());
                paramStm.setString(4, x.getNom());
                paramStm.setString(5, x.getPrenom());
                paramStm.setString(6, x.getRole());

                int nbLignesAffectees= paramStm.executeUpdate();

                if (nbLignesAffectees>0) {
                    switch(x.getRole()){
                        case "etudiant":
                            EtudiantDAO etudiantDAO = new EtudiantDAO(cnx);
                            success=etudiantDAO.create(x);
                            break;
                        case "employeur":
                            EmployeurDAO employeurDAO = new EmployeurDAO(cnx);
                            success=employeurDAO.create(x);
                            break;
                        case "coordonateur":
                            CoordonnateurDAO coordonnateurDAO = new CoordonnateurDAO(cnx);
                            success=coordonnateurDAO.create(x);
                            break;
                    }
                        paramStm.close();
                        return true;
                }
              }
              paramStm.close();
        }
        catch (SQLException ex) {
             Logger.getLogger(UtilisateurDAO.class.getName())
                            .log(Level.SEVERE, null, ex);
            
        }
         return false;
    }

    @Override
    public Utilisateur read(String id) {
        String req = "SELECT * FROM utilisateur WHERE `ID_UTILISATEUR` = ?";

        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);

            paramStm.setString(1, id);

            ResultSet resultat = paramStm.executeQuery();

            // On vérifie s'il y a un résultat
            if(resultat.next()){

                Utilisateur user = new Utilisateur();

                user.setIdUtilisateur(resultat.getString("ID_UTILISATEUR"));
                user.setEmail(resultat.getString("COURRIEL"));
                user.setMotDePasse(resultat.getString("MOT_DE_PASSE"));
                user.setNom(resultat.getString("NOM"));
                user.setPrenom(resultat.getString("PRENOM"));
                user.setRole(resultat.getString("TYPE_UTILISATEUR"));
                switch(user.getRole()){
                    case "etudiant":
                        Utilisateur etudiant = new Utilisateur();
                        EtudiantDAO etudiantDAO = new EtudiantDAO(cnx);
                        etudiant = etudiantDAO.read(id);
                        user.setStatutRecherche(etudiant.getStatutRecherche());
                        user.setListeCritere(etudiant.getListeCritere());
                        user.setListe_candidature(etudiant.getListe_candidature());
                        break;
                    case "employeur":
                        Utilisateur employeur = new Utilisateur();
                        EmployeurDAO employeurDAO = new EmployeurDAO(cnx);
                        employeur = employeurDAO.read(id);
                        //user.setTelephone(employeur.getTelephone());
                        //user.getCompagnie().setIdCompagnie(employeur.getCompagnie().getIdCompagnie());
                        break;
                    case "coordonateur":
                        break;
                }

                resultat.close();
                paramStm.close();
                    return user;
            }

            resultat.close();
            paramStm.close();

        }
        catch (SQLException ex) {
            Logger.getLogger(UtilisateurDAO.class.getName())
                            .log(Level.SEVERE, null, ex);
            
            System.out.println("Erreur dans le read utilisateurDAO"+ex);
        }
        return null;

    }

    @Override
    public boolean update(Utilisateur x) {
                String req = "UPDATE utilisateur SET `COURRIEL` = ?,"
                           + "`NOM` = ?, `PRENOM` = ? WHERE `ID_UTILISATEUR` = ?";

        PreparedStatement paramStm = null;

        boolean success;
        try {
            paramStm = cnx.prepareStatement(req);

                paramStm.setString(1, x.getEmail());
                paramStm.setString(2, x.getNom());
                paramStm.setString(3, x.getPrenom());
                paramStm.setString(4, x.getIdUtilisateur());
                int nbLignesAffectees= paramStm.executeUpdate();

                if (nbLignesAffectees>0) {
                    switch(x.getRole()){

                        case "etudiant":
                            EtudiantDAO etudiantDAO = new EtudiantDAO(cnx);
                            success = etudiantDAO.update(x);
                            break;
                        case "employeur":
                            EmployeurDAO employeurDAO = new EmployeurDAO(cnx);
                            success = employeurDAO.update(x);
                            break;
                        case "coordonateur":
                            CoordonnateurDAO coordonnateurDAO = new CoordonnateurDAO(cnx);
                            success = coordonnateurDAO.update(x);
                            break;
                    }
                    paramStm.close();
                    return true;
                }
                paramStm.close();
                   return false;
            }
        catch (SQLException ex) {
             Logger.getLogger(UtilisateurDAO.class.getName())
                            .log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public boolean delete(Utilisateur x) {
        String req = "DELETE FROM utilisateur WHERE `ID_UTILISATEUR` = ?";

        PreparedStatement paramStm = null;
        boolean success;
        try {
                paramStm = cnx.prepareStatement(req);
                paramStm.setString(1, x.getIdUtilisateur());

                int nbLignesAffectees= paramStm.executeUpdate();

                if (nbLignesAffectees>0) {
                    switch(x.getRole()){

                        case "etudiant":
                            EtudiantDAO etudiantDAO = new EtudiantDAO(cnx);
                            success = etudiantDAO.delete(x);
                            break;
                        case "employeur":
                            EmployeurDAO employeurDAO = new EmployeurDAO(cnx);
                            success = employeurDAO.delete(x);
                            break;
                        case "coordonateur":
                            CoordonnateurDAO coordonnateurDAO = new CoordonnateurDAO(cnx);
                            success = coordonnateurDAO.delete(x);
                            break;
                    }

                    paramStm.close();
                    return true;
                }
                paramStm.close();
        }
        catch (SQLException ex) {
             Logger.getLogger(UtilisateurDAO.class.getName())
                            .log(Level.SEVERE, null, ex);
        }
        return false;
    }

    @Override
    public List<Utilisateur> findAll() {

        List<Utilisateur> liste = new LinkedList<>();
        try {
            Statement stm = cnx.createStatement();
            ResultSet r = stm.executeQuery("SELECT * FROM utilisateur ORDER BY NOM");
            while (r.next()) {
                Utilisateur user = new Utilisateur();
                user.setIdUtilisateur(r.getString("ID_UTILISATEUR"));
                user.setEmail(r.getString("COURRIEL"));
                user.setMotDePasse(r.getString("MOT_DE_PASSE"));
                user.setNom(r.getString("NOM"));
                user.setPrenom(r.getString("PRENOM"));
                user.setRole(r.getString("TYPE_UTILISATEUR"));

                switch(user.getRole()){

                    case "etudiant":
                        Utilisateur etudiant = new Utilisateur();
                        EtudiantDAO etudiantDAO = new EtudiantDAO(cnx);
                        etudiant = etudiantDAO.read(user.getIdUtilisateur());
                        user.setStatutRecherche(etudiant.getStatutRecherche());
                        user.setListeCritere(etudiant.getListeCritere());
                        break;
                    case "employeur":
                        Utilisateur employeur = new Utilisateur();
                        EmployeurDAO employeurDAO = new EmployeurDAO(cnx);
                        employeur = employeurDAO.read(user.getIdUtilisateur());
                        //user.setTelephone(employeur.getTelephone());
                        //user.getCompagnie().setIdCompagnie(employeur.getCompagnie().getIdCompagnie());
                        break;
                    case "coordonateur":
                        break;
                    }

                liste.add(user);
            }
            r.close();
            stm.close();
        }
        catch (SQLException ex){
             Logger.getLogger(UtilisateurDAO.class.getName())
                            .log(Level.SEVERE, null, ex);
        }
        return liste;

    }

    public Utilisateur findByIdentifiantMotPasse(String courriel, String motPasse){

        String req = "SELECT * FROM utilisateur WHERE `COURRIEL` = ? and `MOT_DE_PASSE` = ?";
        ResultSet resultat;

        PreparedStatement paramStm = null;
        try {

                paramStm = cnx.prepareStatement(req);

                paramStm.setString(1, courriel);
                paramStm.setString(2, motPasse);

                resultat = paramStm.executeQuery();

                // On vérifie s'il y a un résultat
                if(resultat.next()){
                    //Vérification si la casse est bien respecté
                    //@Todo:Verifier mots de passe dans la vue

                    Utilisateur user = new Utilisateur();
                    user.setIdUtilisateur(resultat.getString("ID_UTILISATEUR"));
                    user.setEmail(resultat.getString("COURRIEL"));
                    user.setMotDePasse(resultat.getString("MOT_DE_PASSE"));
                    user.setNom(resultat.getString("NOM"));
                    user.setPrenom(resultat.getString("PRENOM"));
                    user.setRole(resultat.getString("TYPE_UTILISATEUR"));

                    switch(user.getRole()){
                        case "etudiant":
                            Utilisateur etudiant = new Utilisateur();
                            EtudiantDAO etudiantDAO = new EtudiantDAO(cnx);
                            etudiant = etudiantDAO.read(user.getIdUtilisateur());
                            user.setStatutRecherche(etudiant.getStatutRecherche());
                            break;
                        case "employeur":
                            Utilisateur employeur = new Utilisateur();
                            EmployeurDAO employeurDAO = new EmployeurDAO(cnx);
                            employeur = employeurDAO.read(user.getIdUtilisateur());
                            //user.setTelephone(employeur.getTelephone());
                            //user.getCompagnie().setIdCompagnie(employeur.getCompagnie().getIdCompagnie());
                            break;
                        case "coordonateur":
                            break;
                        }

                    resultat.close();
                    paramStm.close();
                        return user;
                }
            resultat.close();
            paramStm.close();
            return null;

        }
        catch (SQLException ex) {
            Logger.getLogger(UtilisateurDAO.class.getName())
                            .log(Level.SEVERE, null, ex);
        }

        return null;
    }


    public Utilisateur findByCourriel(String courriel) {
        String req = "SELECT * FROM utilisateur WHERE `COURRIEL` = ?";

        PreparedStatement paramStm = null;
        try {
            paramStm = cnx.prepareStatement(req);

            paramStm.setString(1, courriel);

            ResultSet resultat = paramStm.executeQuery();

            // On vérifie s'il y a un résultat
            if(resultat.next()){

                Utilisateur user = new Utilisateur();
                user.setIdUtilisateur(resultat.getString("ID_UTILISATEUR"));
                user.setEmail(resultat.getString("COURRIEL"));
                user.setMotDePasse(resultat.getString("MOT_DE_PASSE"));
                user.setNom(resultat.getString("NOM"));
                user.setPrenom(resultat.getString("PRENOM"));
                user.setRole(resultat.getString("TYPE_UTILISATEUR"));

                switch(user.getRole()){

                    case "etudiant":
                        Utilisateur etudiant = new Utilisateur();
                        EtudiantDAO etudiantDAO = new EtudiantDAO(cnx);
                        etudiant = etudiantDAO.read(user.getIdUtilisateur());
                        if(etudiant != null)
                        user.setStatutRecherche(etudiant.getStatutRecherche());
                        break;
                    case "employeur":
                        Utilisateur employeur = new Utilisateur();
                        EmployeurDAO employeurDAO = new EmployeurDAO(cnx);
                        employeur = employeurDAO.read(user.getIdUtilisateur());
                        if(employeur != null){
                          //user.setTelephone(employeur.getTelephone());
                          //user.getCompagnie().setIdCompagnie(employeur.getCompagnie().getIdCompagnie());
                        }
                        break;
                    case "coordonateur":
                        break;
                }

                resultat.close();
                paramStm.close();
                return user;
            }

            resultat.close();
            paramStm.close();
        }
        catch (SQLException ex) {
         Logger.getLogger(UtilisateurDAO.class.getName())
                            .log(Level.SEVERE, null, ex);}
        return null;
    }

    @Override
    public Utilisateur read(int id) {
        return new Utilisateur();
    }
}
