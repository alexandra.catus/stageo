/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.model;

import com.util.Util;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Chris
 */
public class AdresseDAO extends DAO<Adresse>{

    public AdresseDAO() {
    }

    public AdresseDAO(Connection cnx) {
        super(cnx);
    }

    @Override
    public boolean create(Adresse x) {
        String req = "INSERT INTO adresse (`ID_ADRESSE`, `NUMERO_CIVIQUE`, `RUE`, `BUREAU`, `VILLE`, `CODE_POSTAL`, `PROVINCE`, `PAYS`, `TEL`) VALUES (?,?,?,?,?,?,?,?,?)";

        PreparedStatement paramStm = null;
        try {
            System.out.println(x.getIdAdresse());
            paramStm = cnx.prepareStatement(req);
              paramStm.setString(1, Util.toUTF8(x.getIdAdresse()));
              paramStm.setString(2, Util.toUTF8(x.getNumeroCivique()));
              paramStm.setString(3, Util.toUTF8(x.getRue()));
              paramStm.setString(4, Util.toUTF8(x.getBureau()));
              paramStm.setString(5, Util.toUTF8(x.getVille()));
              paramStm.setString(6, Util.toUTF8(x.getCodePostal()));
              paramStm.setString(7, Util.toUTF8(x.getProvince()));
              paramStm.setString(8, Util.toUTF8(x.getPays()));
              paramStm.setString(9, Util.toUTF8(x.getTel()));

              //System.out.println("Je suis dans le create d'AdresseDAO");
            int nbLignesAffectees= paramStm.executeUpdate();
            System.out.println("Je suis dans le create d'AdresseDAO");
            System.out.println(nbLignesAffectees);
            if (nbLignesAffectees>0) {
                    paramStm.close();
                    return true;
            }

            return false;
        }
        catch (SQLException exp) {
        }
        finally {
                try {
                    if (paramStm!=null)
                        paramStm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(AdresseDAO.class.getName())
                            .log(Level.SEVERE, null, ex);
                }

        }
        return false;
    }

    @Override
    public Adresse read(int id) {
        String req = "SELECT * FROM adresse WHERE `ID_ADRESSE` = ?";

        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);

            paramStm.setInt(1, id);

            ResultSet resultat = paramStm.executeQuery();

            // On vérifie s'il y a un résultat
            if(resultat.next()){

                Adresse a = new Adresse();
                a.setIdAdresse(resultat.getString("ID_ADRESSE"));
                a.setNumeroCivique(resultat.getString("NUMERO_CIVIQUE"));
                a.setRue(resultat.getString("RUE"));
                a.setBureau(resultat.getString("BUREAU"));
                a.setVille(resultat.getString("VILLE"));
                a.setCodePostal(resultat.getString("CODE_POSTAL"));
                a.setProvince(resultat.getString("PROVINCE"));
                a.setPays(resultat.getString("PAYS"));
                a.setTel(resultat.getString("TEL"));


                resultat.close();
                paramStm.close();
                    return a;
            }

            resultat.close();
            paramStm.close();
            return null;

        }
        catch (SQLException exp) {
        }
        finally {
            try{
                if (paramStm!=null)
                    paramStm.close();
            }
            catch (SQLException exp) {
            }
             catch (Exception e) {
            }
        }

        return null;
    }

    @Override
    public Adresse read(String id) {

        try{
            return this.read(id);
        }
        catch(NumberFormatException e){
            return null;
        }

    }

    @Override
    public boolean update(Adresse x) {
        String req = "UPDATE adresse SET `NUMERO_CIVIQUE` = ?, `RUE` = ?,`BUREAU` = ?,`VILLE` = ?,`CODE_POSTAL` = ?,`PROVINCE` = ?,`PAYS` = ?,`TEL` = ?, WHERE `ID_ADRESSE` = ?";

        PreparedStatement paramStm = null;
        try {

                paramStm = cnx.prepareStatement(req);

                if(x.getNumeroCivique() == null || "".equals(x.getNumeroCivique().trim()))
                    paramStm.setString(1, null);
                else
                    paramStm.setString(1, x.getNumeroCivique());
                 paramStm.setString(2, x.getRue());
                paramStm.setString(3, x.getBureau());
               paramStm.setString(4, x.getVille());
              paramStm.setString(5, x.getCodePostal());
             paramStm.setString(6, x.getProvince());
            paramStm.setString(7, x.getPays());
           paramStm.setString(8, x.getTel());


                int nbLignesAffectees= paramStm.executeUpdate();

                if (nbLignesAffectees>0) {
                        paramStm.close();
                        return true;
                }

            return false;
        }
        catch (SQLException exp) {
        }
        finally {
                try {
                    if (paramStm!=null)
                        paramStm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(AdresseDAO.class.getName())
                            .log(Level.SEVERE, null, ex);
                }

        }
        return false;
    }

    @Override
    public boolean delete(Adresse x) {
        String req = "DELETE FROM adresse WHERE `ID_ADRESSE` = ?";

        PreparedStatement paramStm = null;
        try {

                paramStm = cnx.prepareStatement(req);
                paramStm.setString(1, x.getIdAdresse());

                int nbLignesAffectees= paramStm.executeUpdate();

                if (nbLignesAffectees>0) {
                        paramStm.close();
                    return true;
                }

            return false;
        }
        catch (SQLException exp) {
        }
        finally {
                try {
                    if (paramStm!=null)
                        paramStm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(AdresseDAO.class.getName())
                            .log(Level.SEVERE, null, ex);
                }

        }
        return false;
    }


    @Override
    public List<Adresse> findAll() {
        List<Adresse> liste = new LinkedList<>();
        try {
            Statement stm = cnx.createStatement();
            ResultSet r = stm.executeQuery("SELECT * FROM adresse");
            while (r.next()) {
                Adresse a = new Adresse();
                a.setIdAdresse(r.getString("ID_ADRESSE"));
                a.setNumeroCivique(r.getString("NUMERO_CIVIQUE"));
                a.setRue(r.getString("RUE"));
                a.setBureau(r.getString("BUREAU"));
                a.setVille(r.getString("VILLE"));
                a.setCodePostal(r.getString("CODE_POSTAL"));
                a.setProvince(r.getString("PROVINCE"));
                a.setPays(r.getString("PAYS"));
                a.setTel(r.getString("TEL"));


                liste.add(a);
            }
           // Collections.sort(liste);
            Collections.reverse(liste);
            r.close();
            stm.close();
        }
        catch (SQLException exp) {
        }
        return liste;
    }

    public Adresse findByVille(String ville) {
        String req = "SELECT * FROM adresse WHERE `VILLE` = ?";

        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);
            paramStm.setString(1, Util.toUTF8(ville));

            ResultSet resultat = paramStm.executeQuery();

            // On vérifie s'il y a un résultat
            if(resultat.next()){

                Adresse a = new Adresse();
                a.setIdAdresse(resultat.getString("ID_ADRESSE"));
                a.setNumeroCivique(resultat.getString("NUMERO_CIVIQUE"));
                a.setRue(resultat.getString("RUE"));
                a.setBureau(resultat.getString("BUREAU"));
                a.setVille(resultat.getString("VILLE"));
                a.setCodePostal(resultat.getString("CODE_POSTAL"));
                a.setProvince(resultat.getString("PROVINCE"));
                a.setPays(resultat.getString("PAYS"));
                a.setTel(resultat.getString("TEL"));
               // c.setAdresse((new CompteDAO(cnx)).countCompteByIdEquipe(resultat.getInt("ID_EQUIPE")));


                resultat.close();
                paramStm.close();
                    return a;
            }

            resultat.close();
            paramStm.close();
            return null;

        }
        catch (SQLException exp) {
        }
        finally {
            try{
                if (paramStm!=null)
                    paramStm.close();
            }
            catch (SQLException exp) {
            }
             catch (Exception e) {
            }
        }

        return null;
    }

    public Adresse findByRue(String rue) {
        String req = "SELECT * FROM adresse WHERE `RUE` = ?";

        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);
            paramStm.setString(1, Util.toUTF8(rue));

            ResultSet resultat = paramStm.executeQuery();

            // On vérifie s'il y a un résultat
            if(resultat.next()){

                Adresse a = new Adresse();
                a.setIdAdresse(resultat.getString("ID_ADRESSE"));
                a.setNumeroCivique(resultat.getString("NUMERO_CIVIQUE"));
                a.setRue(resultat.getString("RUE"));
                a.setBureau(resultat.getString("BUREAU"));
                a.setVille(resultat.getString("VILLE"));
                a.setCodePostal(resultat.getString("CODE_POSTAL"));
                a.setProvince(resultat.getString("PROVINCE"));
                a.setPays(resultat.getString("PAYS"));
                a.setTel(resultat.getString("TEL"));
               // c.setAdresse((new CompteDAO(cnx)).countCompteByIdEquipe(resultat.getInt("ID_EQUIPE")));


                resultat.close();
                paramStm.close();
                    return a;
            }

            resultat.close();
            paramStm.close();
            return null;

        }
        catch (SQLException exp) {
        }
        finally {
            try{
                if (paramStm!=null)
                    paramStm.close();
            }
            catch (SQLException exp) {
            }
             catch (Exception e) {
            }
        }

        return null;
    }
     public Adresse findByNumeroCivique(String numeroCivique) {
        String req = "SELECT * FROM adresse WHERE `NUMERO_CIVIQUE` = ?";

        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);
            paramStm.setString(1, Util.toUTF8(numeroCivique));

            ResultSet resultat = paramStm.executeQuery();

            // On vérifie s'il y a un résultat
            if(resultat.next()){

                Adresse a = new Adresse();
                a.setIdAdresse(resultat.getString("ID_ADRESSE"));
                a.setNumeroCivique(resultat.getString("NUMERO_CIVIQUE"));
                a.setRue(resultat.getString("RUE"));
                a.setBureau(resultat.getString("BUREAU"));
                a.setVille(resultat.getString("VILLE"));
                a.setCodePostal(resultat.getString("CODE_POSTAL"));
                a.setProvince(resultat.getString("PROVINCE"));
                a.setPays(resultat.getString("PAYS"));
                a.setTel(resultat.getString("TEL"));
               // c.setAdresse((new CompteDAO(cnx)).countCompteByIdEquipe(resultat.getInt("ID_EQUIPE")));


                resultat.close();
                paramStm.close();
                    return a;
            }

            resultat.close();
            paramStm.close();
            return null;

        }
        catch (SQLException exp) {
        }
        finally {
            try{
                if (paramStm!=null)
                    paramStm.close();
            }
            catch (SQLException exp) {
            }
             catch (Exception e) {
            }
        }

        return null;
    }
     public Adresse findByCodePostal(String codePostal) {
        String req = "SELECT * FROM adresse WHERE `CODE_POSTAL` = ?";

        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);
            paramStm.setString(1, Util.toUTF8(codePostal));

            ResultSet resultat = paramStm.executeQuery();

            // On vérifie s'il y a un résultat
            if(resultat.next()){

                Adresse a = new Adresse();
                a.setIdAdresse(resultat.getString("ID_ADRESSE"));
                a.setNumeroCivique(resultat.getString("NUMERO_CIVIQUE"));
                a.setRue(resultat.getString("RUE"));
                a.setBureau(resultat.getString("BUREAU"));
                a.setVille(resultat.getString("VILLE"));
                a.setCodePostal(resultat.getString("CODE_POSTAL"));
                a.setProvince(resultat.getString("PROVINCE"));
                a.setPays(resultat.getString("PAYS"));
                a.setTel(resultat.getString("TEL"));
               // c.setAdresse((new CompteDAO(cnx)).countCompteByIdEquipe(resultat.getInt("ID_EQUIPE")));


                resultat.close();
                paramStm.close();
                    return a;
            }

            resultat.close();
            paramStm.close();
            return null;

        }
        catch (SQLException exp) {
        }
        finally {
            try{
                if (paramStm!=null)
                    paramStm.close();
            }
            catch (SQLException exp) {
            }
             catch (Exception e) {
            }
        }

        return null;
    }
     public Adresse findByTel(String tel) {
        String req = "SELECT * FROM adresse WHERE `TEL` = ?";

        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);
            paramStm.setString(1, Util.toUTF8(tel));

            ResultSet resultat = paramStm.executeQuery();

            // On vérifie s'il y a un résultat
            if(resultat.next()){

                Adresse a = new Adresse();
                a.setIdAdresse(resultat.getString("ID_ADRESSE"));
                a.setNumeroCivique(resultat.getString("NUMERO_CIVIQUE"));
                a.setRue(resultat.getString("RUE"));
                a.setBureau(resultat.getString("BUREAU"));
                a.setVille(resultat.getString("VILLE"));
                a.setCodePostal(resultat.getString("CODE_POSTAL"));
                a.setProvince(resultat.getString("PROVINCE"));
                a.setPays(resultat.getString("PAYS"));
                a.setTel(resultat.getString("TEL"));
               // c.setAdresse((new CompteDAO(cnx)).countCompteByIdEquipe(resultat.getInt("ID_EQUIPE")));


                resultat.close();
                paramStm.close();
                    return a;
            }

            resultat.close();
            paramStm.close();
            return null;

        }
        catch (SQLException exp) {
        }
        finally {
            try{
                if (paramStm!=null)
                    paramStm.close();
            }
            catch (SQLException exp) {
            }
             catch (Exception e) {
            }
        }

        return null;
    }
     public Adresse findByID(String idAdresse) {
        String req = "SELECT * FROM adresse WHERE `ID_ADRESSE` = ?";

        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);
            paramStm.setString(1, Util.toUTF8(idAdresse));

            ResultSet resultat = paramStm.executeQuery();

            // On vérifie s'il y a un résultat
            if(resultat.next()){

                Adresse a = new Adresse();
                a.setIdAdresse(resultat.getString("ID_ADRESSE"));
                a.setNumeroCivique(resultat.getString("NUMERO_CIVIQUE"));
                a.setRue(resultat.getString("RUE"));
                a.setBureau(resultat.getString("BUREAU"));
                a.setVille(resultat.getString("VILLE"));
                a.setCodePostal(resultat.getString("CODE_POSTAL"));
                a.setProvince(resultat.getString("PROVINCE"));
                a.setPays(resultat.getString("PAYS"));
                a.setTel(resultat.getString("TEL"));
               // c.setAdresse((new CompteDAO(cnx)).countCompteByIdEquipe(resultat.getInt("ID_EQUIPE")));


                resultat.close();
                paramStm.close();
                    return a;
            }

            resultat.close();
            paramStm.close();
            return null;

        }
        catch (SQLException exp) {
        }
        finally {
            try{
                if (paramStm!=null)
                    paramStm.close();
            }
            catch (SQLException exp) {
            }
             catch (Exception e) {
            }
        }

        return null;
    }
}
