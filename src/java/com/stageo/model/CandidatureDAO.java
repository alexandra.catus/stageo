/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.model;

import com.util.Util;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Chris
 */
public class CandidatureDAO extends DAO<Candidature> {

    public CandidatureDAO() {
    }

    public CandidatureDAO(Connection cnx) {
        super(cnx);
    }

    @Override
    public boolean create(Candidature x) {
        
        String req = "INSERT INTO candidature (`ID_ETUDIANT`,`ID_OFFRE`,`DATE`,`STATUT`) VALUES (?,?,?,?)";

        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);
            paramStm.setString(1, Util.toUTF8(x.getIdEtudiant()));
            paramStm.setString(2, Util.toUTF8(x.getIdOffre()));
            paramStm.setTimestamp(3, (x.getDate()));
            paramStm.setString(4, Util.toUTF8(x.getStatut()));

            System.out.println(x.getIdEtudiant());
            System.out.println(x.getIdOffre());
            System.out.println(x.getDate());
            System.out.println(x.getStatut());
            int nbLignesAffectees = paramStm.executeUpdate();

            if (nbLignesAffectees > 0) {
                paramStm.close();
                return true;
            }

            return false;
        } catch (SQLException exp) {
        } finally {
            try {
                if (paramStm != null) {
                    paramStm.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(CandidatureDAO.class.getName())
                        .log(Level.SEVERE, null, ex);
            }

        }
        return false;
    }

    public Candidature read(int id, int id2) {
        String req = "SELECT * FROM candidature WHERE `ID_ETUDIANT` = ? and `ID_OFFRE` = ?";

        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);

            paramStm.setInt(1, id);

            ResultSet r = paramStm.executeQuery();

            // On vérifie s'il y a un résultat    
            if (r.next()) {

                Candidature c = new Candidature();
                c.setIdEtudiant(r.getString("ID_ETUDIANT"));
                c.setIdOffre(r.getString("ID_OFFRE"));
                c.setDate(r.getTimestamp("DATE"));
                c.setStatut(r.getString("STATUT"));

                r.close();
                paramStm.close();
                return c;
            }

            r.close();
            paramStm.close();
            return null;

        } catch (SQLException exp) {
        } finally {
            try {
                if (paramStm != null) {
                    paramStm.close();
                }
            } catch (SQLException exp) {
            } catch (Exception e) {
            }
        }

        return null;
    }

    public Candidature read(String id, String id2) {

        try {
            return this.read(Integer.parseInt(id), Integer.parseInt(id2));
        } catch (NumberFormatException e) {
            return null;
        }

    }

    @Override
    public boolean update(Candidature x) {
        String req = "UPDATE candidature SET `DATE` = ?,`STATUT` = ?";

        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);

            if (x.getIdEtudiant() == null || "".equals(x.getIdEtudiant().trim()) || x.getIdOffre() == null || "".equals(x.getIdOffre().trim())) {
                paramStm.setString(1, null);
            } else {
                paramStm.setTimestamp(1, x.getDate());
                paramStm.setString(2, x.getStatut());
            }
            int nbLignesAffectees = paramStm.executeUpdate();

            if (nbLignesAffectees > 0) {
                paramStm.close();
                return true;
            }

            return false;
        } catch (SQLException exp) {
        } finally {
            try {
                if (paramStm != null) {
                    paramStm.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(CandidatureDAO.class.getName())
                        .log(Level.SEVERE, null, ex);
            }

        }
        return false;
    }

    @Override
    public boolean delete(Candidature x) {
        String req = "DELETE FROM candidature WHERE `ID_ETUDIANT` = ? and `ID_OFFRE` = ?";

        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);
            paramStm.setString(1, x.getIdEtudiant());
            paramStm.setString(2, x.getIdOffre());

            int nbLignesAffectees = paramStm.executeUpdate();

            if (nbLignesAffectees > 0) {
                paramStm.close();
                return true;
            }

            return false;
        } catch (SQLException exp) {
        } finally {
            try {
                if (paramStm != null) {
                    paramStm.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(CandidatureDAO.class.getName())
                        .log(Level.SEVERE, null, ex);
            }

        }
        return false;
    }

    @Override
    public List<Candidature> findAll() {
        List<Candidature> liste = new LinkedList<>();
        try {
            Statement stm = cnx.createStatement();
            ResultSet r = stm.executeQuery("SELECT * FROM candidature");
            while (r.next()) {
                Candidature c = new Candidature();
                c.setIdEtudiant(r.getString("ID_ETUDIANT"));
                c.setIdOffre(r.getString("ID_OFFRE"));
                c.setDate(r.getTimestamp("DATE"));
                c.setStatut(r.getString("STATUT"));

                liste.add(c);
            }
            // Collections.sort(liste);
            Collections.reverse(liste);
            r.close();
            stm.close();
        } catch (SQLException exp) {
        }
        return liste;
    }

    public List<Candidature> findByIdOffre(String id) {
        String req = "SELECT * FROM candidature WHERE `ID_OFFRE` = ?";

        PreparedStatement paramStm = null;
        List<Candidature> liste = new LinkedList<>();

        try {

            paramStm = cnx.prepareStatement(req);
            paramStm.setString(1, Util.toUTF8(id));

            ResultSet r = paramStm.executeQuery();

            // On vérifie s'il y a un résultat    
            if (r.next()) {

                Candidature c = new Candidature();
                c.setIdEtudiant(r.getString("ID_ETUDIANT"));
                c.setIdOffre(r.getString("ID_OFFRE"));
                c.setDate(r.getTimestamp("DATE"));
                c.setStatut(r.getString("STATUT"));

                liste.add(c);

            }

            r.close();
            paramStm.close();
            return liste;

        } catch (SQLException exp) {
        } finally {
            try {
                if (paramStm != null) {
                    paramStm.close();
                }
            } catch (SQLException exp) {
            } catch (Exception e) {
            }
        }

        return null;
    }

    public List<Candidature> findByStudent(String id) {
        System.out.println("test ");
        String req = "SELECT * FROM candidature WHERE `ID_ETUDIANT` = ?";
        PreparedStatement paramStm = null;
        List<Candidature> liste = new LinkedList<>();
       
        try {

            paramStm = cnx.prepareStatement(req);
            paramStm.setString(1, Util.toUTF8(id));

            ResultSet r = paramStm.executeQuery();
             System.out.println("test ");
            // On vérifie s'il y a un résultat    
            if (r.next()) {
                 System.out.println(r);
                Candidature c = new Candidature();
                c.setIdEtudiant(r.getString("ID_ETUDIANT"));
                c.setIdOffre(r.getString("ID_OFFRE"));
                c.setDate(r.getTimestamp("DATE"));
                c.setStatut(r.getString("STATUT"));

                liste.add(c);

            }

            r.close();
            paramStm.close();
            return liste;

        } catch (SQLException ex) {
            Logger.getLogger(CoordonnateurDAO.class.getName())
                    .log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(CoordonnateurDAO.class.getName())
                    .log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public Candidature read(int id) {
        throw new UnsupportedOperationException("clef composee");
    }

    @Override
    public Candidature read(String id) {
        throw new UnsupportedOperationException("clef composee");
    }

}
