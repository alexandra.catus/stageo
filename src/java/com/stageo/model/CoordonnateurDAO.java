/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.model;

import com.util.Util;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dave
 */
public class CoordonnateurDAO extends DAO<Utilisateur>{

    public CoordonnateurDAO() {
    }

    public CoordonnateurDAO(Connection cnx) {
        super(cnx);
    }

    @Override
    public boolean create(Utilisateur x) {
        String req = "INSERT INTO coordonnateur (`ID_COORDONNATEUR`) VALUES (?)";

        PreparedStatement paramStm = null;
        try {

                paramStm = cnx.prepareStatement(req);

              if(x.getIdUtilisateur() != null && !"".equals(x.getIdUtilisateur().trim())){
                paramStm.setString(1, Util.toUTF8(x.getIdUtilisateur()));

                int nbLignesAffectees= paramStm.executeUpdate();

                if (nbLignesAffectees>0) {
                        paramStm.close();
                        return true;
                }
              }
            return false;
        }
        catch (SQLException exp) {
        }
        finally {
                try {
                    if (paramStm!=null)
                        paramStm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(CoordonnateurDAO.class.getName())
                            .log(Level.SEVERE, null, ex);
                }
        }
        return false;
    }

    @Override
    public Utilisateur read(String id) {
        String req = "SELECT * FROM coordonnateur WHERE `ID_COORDONNATEUR` = ?";

        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);

            paramStm.setString(1, id);

            ResultSet resultat = paramStm.executeQuery();

            // On vérifie s'il y a un résultat
            if(resultat.next()){

                Utilisateur coordonnateur = new Utilisateur();

                coordonnateur.setIdUtilisateur(resultat.getString("ID_COORDONNATEUR"));

                resultat.close();
                paramStm.close();
                    return coordonnateur;
            }

            resultat.close();
            paramStm.close();
            return null;

        }
        catch (SQLException exp) {
        }
        finally {
            try{
                if (paramStm!=null)
                    paramStm.close();
            }
            catch (SQLException exp) {
            }
             catch (Exception e) {
            }
        }

        return null;

    }

    /*@Override
    public boolean update(Utilisateur x) {
                String req = "UPDATE coordonnateur SET ID_COORDONNATEUR = ? WHERE ID_COORDONNATEUR = ?";

        PreparedStatement paramStm = null;
        try {
            paramStm = cnx.prepareStatement(req);

            if(x.getIdUtilisateur() != null && !"".equals(x.getIdUtilisateur().trim())){
                paramStm.setString(1, Util.toUTF8(x.getIdUtilisateur()));
                paramStm.setString(2, Util.toUTF8(x.getIdUtilisateur()));

                int nbLignesAffectees= paramStm.executeUpdate();

                if (nbLignesAffectees>0) {
                    paramStm.close();
                    return true;
                }
            }
        return false;
        }
        catch (SQLException exp) {
            System.out.println(exp.getMessage());
        }
        finally {
                try {
                    if (paramStm!=null)
                        paramStm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(CoordonnateurDAO.class.getName())
                            .log(Level.SEVERE, null, ex);
                }

        }
        return false;
    }*/

    @Override
    public boolean delete(Utilisateur x) {
        String req = "DELETE FROM coordonnateur WHERE `ID_COORDONNATEUR` = ?";

        PreparedStatement paramStm = null;

        try {
                paramStm = cnx.prepareStatement(req);
                paramStm.setString(1, x.getIdUtilisateur());

                int nbLignesAffectees= paramStm.executeUpdate();

                if (nbLignesAffectees>0) {
                    paramStm.close();
                    return true;
                }

            return false;
        }
        catch (SQLException exp) {
        }
        catch (Exception exp) {
        }
        finally {
                try {
                    if (paramStm!=null)
                        paramStm.close();
                } catch (SQLException ex) {
                    Logger.getLogger(CoordonnateurDAO.class.getName())
                            .log(Level.SEVERE, null, ex);
                }
        }
        return false;
    }

    @Override
    public List<Utilisateur> findAll() {

        List<Utilisateur> liste = new LinkedList<>();
        try {
            Statement stm = cnx.createStatement();
            ResultSet r = stm.executeQuery("SELECT * FROM coordonnateur");
            while (r.next()) {
                Utilisateur coordonnateur = new Utilisateur();
                coordonnateur.setIdUtilisateur(r.getString("ID_COORDONNATEUR"));

                liste.add(coordonnateur);
            }
            r.close();
            stm.close();
        }
        catch (SQLException exp){
        }
        return liste;

    }

    @Override
    public Utilisateur read(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean update(Utilisateur x) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
