/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.model;

import java.sql.Timestamp;

/**
 *
 * @author Christopher
 */
public class Candidature {
    private String idEtudiant, idOffre, statut;
    private Timestamp date;
    public Candidature() {
    }
    public Candidature(String idEtudiant, String idOffre) {
        this.idEtudiant = idEtudiant;
        this.idOffre = idOffre;
    }
    public Candidature(String idEtudiant, String idOffre, Timestamp date, String statut) {
        this.idEtudiant = idEtudiant;
        this.idOffre = idOffre;
        this.date = date;
        this.statut = statut;
    }
    public Candidature(String idOffre, Timestamp date, String statut) {
        this.idOffre = idOffre;
        this.date = date;
        this.statut = statut;
    }
    public String getIdEtudiant() {
        return idEtudiant;
    }

    public void setIdEtudiant(String idEtudiant) {
        this.idEtudiant = idEtudiant;
    }

    public String getIdOffre() {
        return idOffre;
    }

    public void setIdOffre(String idOffre) {
        this.idOffre = idOffre;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }
    
}
