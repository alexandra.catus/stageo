/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.model;

import com.util.Util;

import com.util.Util;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dave
 */
public class EmployeurDAO extends DAO<Utilisateur> {

    public EmployeurDAO() {
    }

    public EmployeurDAO(Connection cnx) {
        super(cnx);
    }

    @Override
    public boolean create(Utilisateur x) {
        String req = "INSERT INTO employeur (`ID_EMPLOYEUR`, `TEL`, `ID_COMPAGNIE`) VALUES (?,?,?)";

        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);

            if (x.getIdUtilisateur() != null && !"".equals(x.getIdUtilisateur().trim())) {
                paramStm.setString(1, Util.toUTF8(x.getIdUtilisateur()));

                if (x.getTelephone() == null || "".equals(x.getTelephone().trim())) {
                    paramStm.setString(2, "(XXX)XXX-XXXX");
                } else {
                    paramStm.setString(2, x.getTelephone());
                }

                if (x.getCompagnie() == null) {
                    paramStm.setString(3, "Undefined");
                } else {
                    paramStm.setString(3, x.getCompagnie().getIdCompagnie());
                }

                // Création/Initialisation de la compagnie de l'employeur
                CompagnieDAO compagnieDAO = new CompagnieDAO(cnx);
                boolean success;
                success = compagnieDAO.create(x.getCompagnie());

                int nbLignesAffectees = paramStm.executeUpdate();

                System.out.println("Je suis dans le create d'EmployeurDAO");
                if (nbLignesAffectees > 0) {
                    paramStm.close();
                    return true;
                }
            }
            return false;
        } catch (SQLException exp) {
        } finally {
            try {
                if (paramStm != null) {
                    paramStm.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(EmployeurDAO.class.getName())
                        .log(Level.SEVERE, null, ex);
            }
        }
        return false;
    }

    @Override
    public Utilisateur read(String id) {
        String req = "SELECT * FROM employeur WHERE `ID_EMPLOYEUR` = ?";

        PreparedStatement paramStm = null;
        try {
            paramStm = cnx.prepareStatement(req);
            paramStm.setString(1, id);
            ResultSet resultat = paramStm.executeQuery();

            // On vérifie s'il y a un résultat
            if (resultat.next()) {

                Utilisateur employeur = new Utilisateur();
                employeur.setIdUtilisateur(resultat.getString("ID_EMPLOYEUR"));

                employeur.getCompagnie().setIdCompagnie(resultat.getString("ID_COMPAGNIE"));
                

                resultat.close();
                paramStm.close();
                return employeur;
            }

            resultat.close();
            paramStm.close();
            return null;

        } catch (SQLException exp) {
        } finally {
            try {
                if (paramStm != null) {
                    paramStm.close();
                }
            } catch (SQLException exp) {
            } catch (Exception e) {
            }
        }

        return null;

    }

    @Override
    public boolean update(Utilisateur x) {
        String req = "UPDATE employeur SET `TELEPHONE` = ?, `ID_COMPAGNIE` = ? WHERE `ID_EMPLOYEUR` = ?";

        PreparedStatement paramStm = null;
        try {
            paramStm = cnx.prepareStatement(req);

            if (x.getIdUtilisateur() != null && !"".equals(x.getIdUtilisateur().trim())) {

                if (x.getTelephone() == null || "".equals(x.getTelephone().trim())) {
                    paramStm.setString(1, "");
                } else {
                    paramStm.setString(1, x.getTelephone());
                }
                if (x.getCompagnie() == null) {
                    paramStm.setString(2, "");
                } else {
                    paramStm.setString(2, x.getCompagnie().getIdCompagnie());
                }

                paramStm.setString(3, Util.toUTF8(x.getIdUtilisateur()));

                int nbLignesAffectees = paramStm.executeUpdate();

                if (nbLignesAffectees > 0) {
                    paramStm.close();
                    return true;
                }
            }
            return false;
        } catch (SQLException exp) {
            System.out.println(exp.getMessage());
        } finally {
            try {
                if (paramStm != null) {
                    paramStm.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(EmployeurDAO.class.getName())
                        .log(Level.SEVERE, null, ex);
            }

        }
        return false;
    }

    @Override
    public boolean delete(Utilisateur x) {
        String req = "DELETE FROM employeur WHERE `ID_EMPLOYEUR` = ?";

        PreparedStatement paramStm = null;

        try {
            paramStm = cnx.prepareStatement(req);
            paramStm.setString(1, x.getIdUtilisateur());

            int nbLignesAffectees = paramStm.executeUpdate();

            if (nbLignesAffectees > 0) {
                paramStm.close();
                return true;
            }

            return false;
        } catch (SQLException exp) {
        } catch (Exception exp) {
        } finally {
            try {
                if (paramStm != null) {
                    paramStm.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(EmployeurDAO.class.getName())
                        .log(Level.SEVERE, null, ex);
            }
        }
        return false;
    }

    @Override
    public List<Utilisateur> findAll() {

        List<Utilisateur> liste = new LinkedList<>();
        try {
            Statement stm = cnx.createStatement();
            ResultSet r = stm.executeQuery("SELECT * FROM employeur");
            while (r.next()) {
                Utilisateur employeur = new Utilisateur();
                employeur.setIdUtilisateur(r.getString("ID_EMPLOYEUR"));
                employeur.setTelephone(r.getString("TELEPHONE"));
                employeur.getCompagnie().setIdCompagnie(r.getString("ID_COMPAGNIE"));

                liste.add(employeur);
            }
            r.close();
            stm.close();
        } catch (SQLException exp) {
        }
        return liste;

    }

    public Utilisateur findByTelephone(String telephone) {

        String req = "SELECT * FROM employeur WHERE `TELEPHONE` = ?";
        ResultSet resultat;

        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);

            paramStm.setString(1, Util.toUTF8(telephone));

            resultat = paramStm.executeQuery();

            // On vérifie s'il y a un résultat
            if (resultat.next()) {
                //Vérification si la casse est bien respecté
                if (!Util.toUTF8(telephone).equals(resultat.getString("TELEPHONE"))) {
                    return null;
                }

                Utilisateur employeur = new Utilisateur();
                employeur.setIdUtilisateur(resultat.getString("ID_EMPLOYEUR"));
                employeur.setTelephone(resultat.getString("TELEPHONE"));
                employeur.getCompagnie().setIdCompagnie(resultat.getString("ID_COMPAGNIE"));

                resultat.close();
                paramStm.close();
                return employeur;
            }
            resultat.close();
            paramStm.close();
            return null;

        } catch (SQLException exp) {
        } finally {
            try {
                if (paramStm != null) {
                    paramStm.close();
                }
            } catch (SQLException exp) {
            } catch (Exception e) {
            }
        }

        return null;
    }

    public Utilisateur findByCourriel(String idCompagnie) {
        String req = "SELECT * FROM employeur WHERE `ID_COMPAGNIE` = ?";

        PreparedStatement paramStm = null;
        try {

            paramStm = cnx.prepareStatement(req);

            paramStm.setString(1, Util.toUTF8(idCompagnie));

            ResultSet resultat = paramStm.executeQuery();

            // On vérifie s'il y a un résultat
            if (resultat.next()) {

                Utilisateur employeur = new Utilisateur();
                employeur.setIdUtilisateur(resultat.getString("ID_EMPLOYEUR"));
                employeur.setTelephone(resultat.getString("TELEPHONE"));
                employeur.getCompagnie().setIdCompagnie(resultat.getString("ID_COMPAGNIE"));

                resultat.close();
                paramStm.close();
                return employeur;
            }

            resultat.close();
            paramStm.close();
            return null;
        } catch (SQLException exp) {
        } finally {
            try {
                if (paramStm != null) {
                    paramStm.close();
                }
            } catch (SQLException exp) {
            }
        }
        return null;
    }

    @Override
    public Utilisateur read(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
