/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.model;

import java.util.UUID;

/**
 *
 * @author JP
 */
public class Adresse {

    private String IdAdresse,
            numeroCivique = "0000",
            rue = "nom de la rue",
            bureau = "0001",
            ville = "ville",
            codePostal = "code postal",
            province = "province",
            pays = "pays",
            tel = "XXX-XXXX";
    
    @Override
    public String toString() {
        String adresse = this.numeroCivique+""+this.rue+""+this.ville+""+this.codePostal+""+this.province;
        return adresse;
    }

    //Constructeurs
    public Adresse() {
        String uniqueID = UUID.randomUUID().toString();
        this.IdAdresse = uniqueID;
    }

    public Adresse(String numeroCivique, String rue, String bureau, String ville, String codePostal, String province, String pays, String tel) {
        String uniqueID = UUID.randomUUID().toString();
        this.IdAdresse = uniqueID;
        this.numeroCivique = numeroCivique;
        this.rue = rue;
        this.bureau = bureau;
        this.ville = ville;
        this.codePostal = codePostal;
        this.province = province;
        this.pays = pays;
        this.tel = tel;
    }

    public Adresse(String IdAdresse, String numeroCivique, String rue, String bureau, String ville, String codePostal, String province, String pays, String tel) {
        this.IdAdresse = IdAdresse;
        this.numeroCivique = numeroCivique;
        this.rue = rue;
        this.bureau = bureau;
        this.ville = ville;
        this.codePostal = codePostal;
        this.province = province;
        this.pays = pays;
        this.tel = tel;
    }

    public String getIdAdresse() {
        return IdAdresse;
    }

    public void setIdAdresse(String IdAdresse) {
        this.IdAdresse = IdAdresse;
    }

    public String getNumeroCivique() {
        return numeroCivique;
    }

    public void setNumeroCivique(String numeroCivique) {
        this.numeroCivique = numeroCivique;
    }

    public String getRue() {
        return rue;
    }

    public void setRue(String rue) {
        this.rue = rue;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getBureau() {
        return bureau;
    }

    public void setBureau(String bureau) {
        this.bureau = bureau;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

}
