/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.controleur;
import com.stageo.model.Critere;
import com.stageo.model.CritereDAO;
import com.util.Util;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import com.stageo.model.Utilisateur;
import com.stageo.model.UtilisateurDAO;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import jdbc.Config;
import jdbc.Connexion;

/**
 *
 * @author JP
 */
public class EffectuerModificationUtilisateurAction implements Action, RequestAware, RequirePRGAction, SessionAware, DataSender{
    

    private HttpServletResponse response;
    private HttpServletRequest request;
    private HttpSession session;
    private HashMap data;

    @Override
    public String execute() {
        String action = "*.do?tache=afficherPageProfil";
        session = request.getSession(true);
        
        String idUtilisateur = (String)session.getAttribute("connecte"),
               email = request.getParameter("email"),
               prenom = request.getParameter("prenom"),
               nom = request.getParameter("nom"),
               role = (String) session.getAttribute("role"),
               statutRecherche= request.getParameter("statutRecherche");
               List<Critere> listeCritereEtudiant= new ArrayList<>();
               List<Critere> ListeCritere = null;
                          
            boolean erreurSurvenue = false;
            try {
                 
                Connection cnx = Connexion.startConnection(Config.DB_USER, Config.DB_PWD, Config.URL, Config.DRIVER);

                UtilisateurDAO dao = new UtilisateurDAO(cnx);
                Utilisateur user = dao.read(idUtilisateur);
                if (role.equals("etudiant")){
                    System.out.println("Je suis un etudiant");
                     user.setStatutRecherche(statutRecherche);
                CritereDAO cDAO= new CritereDAO(cnx);
                ListeCritere= cDAO.findAll();
                for (int i = 0; i < ListeCritere.size(); i++){
                    
                    String checked = request.getParameter(ListeCritere.get(i).getIdCritere());
                    //verifie que la valeur est cochée
                    if (checked!=null){
                    listeCritereEtudiant.add(ListeCritere.get(i));
                    }
                }
                        user.setListeCritere(listeCritereEtudiant);
                }
                
                // @Todo: Implémenter l'update de compte pour les compagnies
                        user.setEmail(email);
                        user.setPrenom(prenom);
                        user.setNom(nom);
                        
                System.out.println(user);
                        dao.update(user);
                        
            } catch (ClassNotFoundException | SQLException ex) {
                System.out.println(ex);
               Logger.getLogger(EffectuerModificationUtilisateurAction.class.getName()).log(Level.SEVERE, null, ex);
           }
            return action;
        }
            
  

    @Override
    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    @Override
    public void setResponse(HttpServletResponse response) {
        this.response = response;
    }

    @Override
    public void setData(Map<String, Object> data) {
        this.data = (HashMap) data;
    }

    @Override
    public void setSession(HttpSession session) {
        this.session = session;
    }

}
