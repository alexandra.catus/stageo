
package com.stageo.controleur;

import javax.servlet.http.HttpSession;
/**
 *
 * @author Dave
 */
public interface SessionAware {
    public void setSession(HttpSession session);
}
