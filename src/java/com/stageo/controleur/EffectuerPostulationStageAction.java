package com.stageo.controleur;

import com.util.Util;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import com.stageo.model.Utilisateur;
import com.stageo.model.UtilisateurDAO;
import com.stageo.model.Candidature;
import com.stageo.model.CandidatureDAO;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import jdbc.Config;
import jdbc.Connexion;

/**
 *
 * @author JP
 */
public class EffectuerPostulationStageAction implements Action, RequestAware, RequirePRGAction, SessionAware, DataSender{
    

   private HttpServletRequest request;
    private HttpSession session;
    private HttpServletResponse response;
    private HashMap data;

    @Override
    public String execute() {
        

       
        
     
        String action = "*.do?tache=afficherPageListeStage";
        //request.setAttribute("vue", "accueil.jsp");

             
               System.out.println("Test1");
            System.out.println(request.getParameter("idOffre"));
        if((request.getParameter("idOffre") != null)){
              System.out.println("Test2");
             
            try{
                
                Connection cnx = Connexion.startConnection(Config.DB_USER,Config.DB_PWD,Config.URL,Config.DRIVER);
                session = request.getSession(true);
                 
                UtilisateurDAO dao = new UtilisateurDAO(cnx);
                Utilisateur user = dao.read((String) session.getAttribute("connecte"));
                
                CandidatureDAO canDao = new CandidatureDAO(cnx);
                Candidature can = new Candidature();
                
                String  idOffre = request.getParameter("idOffre"),
                        idUser = user.getIdUtilisateur();
                 
             
                Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                String dateDeCreation  = dateFormat.format(timestamp);
               

            
                can.setIdOffre(idOffre);
                can.setIdEtudiant(idUser);
                can.setDate(timestamp);
                can.setStatut("En attente");
                canDao.create(can);
                
               System.out.println("Candidature A ETE CRÉÉ");       
            }
            catch(ClassNotFoundException e){
                System.out.println("Erreur dans le chargement du pilote :"+ e);
            } catch (SQLException ex) {
                Logger.getLogger(EffectuerPostulationStageAction.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return action;

    }

    @Override
    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }
    
    @Override
    public void setResponse(HttpServletResponse response) {
        this.response = response;
    }

    @Override
    public void setSession(HttpSession session) {
        this.session = session;
    }

    @Override
    public void setData(Map<String, Object> data) {
        this.data = (HashMap) data;
    }   
}
