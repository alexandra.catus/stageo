/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.controleur;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Dave
 */
public class DefaultAction implements Action, RequestAware, SessionAware{

    private HttpServletRequest request;
    private HttpServletResponse response;
     private HttpSession session;
    
    @Override
    public String execute() {
        String page="page-accueil.jsp"; 
        if(session.getAttribute("connecte") != null && session.getAttribute("role") != null)
            page="page-accueil-connecte.jsp";
        request.setAttribute("vue", page);
        return "/index.jsp";
    }
    
    @Override
    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }
    
    @Override
    public void setResponse(HttpServletResponse response) {
        this.response = response;
    }

    @Override
    public void setSession(HttpSession session) {
         this.session = session;
    }
}
