/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.controleur;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import jdbc.Config;
import jdbc.Connexion;
import com.stageo.model.Utilisateur;
import com.stageo.model.UtilisateurDAO;
import com.util.Util;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author JP
 */
public class EffectuerConnexionAction implements Action, RequestAware, SessionAware, RequirePRGAction, DataSender {

    private HttpSession session;
    private HttpServletRequest request;
    private HttpServletResponse response;
    private HashMap data;

    @Override
    public String execute() {
          System.out.println("enfin");
        String action = "*.do?tache=afficherPageConnexion";
            String identifiant = request.getParameter("courrielConnexion"),
                    motPasse = request.getParameter("motDePasseConnexion");
            
            try {
                UtilisateurDAO dao
                        = new UtilisateurDAO(Connexion.startConnection(Config.DB_USER, Config.DB_PWD, Config.URL, Config.DRIVER));
                Utilisateur user = dao.findByIdentifiantMotPasse(identifiant, motPasse);
             
                // On vérifie s'il y a un résultat et si le user est un etudiant    
                if (user != null){
                    session = request.getSession(true);
                    session.setAttribute("connecte", user.getIdUtilisateur());
                    session.setAttribute("role", user.getRole());
                    return "*.do?tache=afficherPageProfil";

                }
                    
                else {
                    data.put("echecConnexion", "L'identifiant et/ou le mot de passe entré est invalide");
                    data.put("courrielConnexion", Util.toUTF8(identifiant));
                   
                   
                }
            } catch (ClassNotFoundException e) {
                System.out.println("Erreur dans le chargement du pilote :" + e);

            } catch (SQLException ex) {
                Logger.getLogger(EffectuerConnexionAction.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                Connexion.close();
            }
        return action;
    }

    @Override
    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    @Override
    public void setResponse(HttpServletResponse response) {
        this.response = response;
    }

    @Override
    public void setSession(HttpSession session) {
        this.session = session;
    }

    @Override
    public void setData(Map<String, Object> data) {
        this.data = (HashMap) data;
    }

}
