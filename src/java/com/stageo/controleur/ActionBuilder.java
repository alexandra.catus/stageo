/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.controleur;

/**
 *
 * @author AC
 */
public class ActionBuilder {

    public static Action getAction(String actionName) {
       //System.out.println("je sduis dans le actionbuilder");
        //System.out.print("entrer dans l'action builder");
        if (actionName != null) {
            switch (actionName) {
                
                case "afficherPageInscription":
                    return new AfficherPageInscription();
//Stage         
              
                case "afficherPageListeStage":
                    return new AfficherPageListeStage();
                case "afficherPageEntreprise":
                    return new AfficherPageEntreprise();
                case "afficherPageListeStagiaires":
                    return new AfficherPageListeStagiaires();
                case "afficherListeCompetences":
                    return new AfficherListeCompetences();
                case "ajouterOffreDeStage":
                    return new AjouterOffreDeStage();
                case "ajoutCritere":
                     return new AjouterCritereAction();
                case "supprimerCritere":
                     return new SupprimerCritere();
                case "supprimerOffreDeStage":
                    return new SupprimerOffreDeStage();
                case "modifierOffreDeStage":
                    return new ModifierOffreDeStage();
                case "suggererCandidature":
                    return new SuggererCandidature();
                case "accepterCandidature":
                    return new AccepterCandidature();  
                case "effectuerPostulationAction":
                    return new EffectuerPostulationStageAction(); 
//Profil
                case "afficherPageCandidaturesEtudiant":
                    return new AfficherPageCandidatures();
                case "afficherPageProfil":
                    return new AfficherPageProfil();
                case "effectuerModificationUtilisateur":
                    return new EffectuerModificationUtilisateurAction();
                case "modifierProfilEtudiant":
                    return new ModifierProfilEtudiant();
//Documents
                case "afficherPageDocument":
                    return new AfficherPageDocument();
                case "ajouterDocument":
                    return new AjouterDocumentAction();
                case "modifierDocument":
                    return new ModifierDocument();   
                case "supprimerDocument":
                    return new SupprimerDocumentAction(); 
                    
// Connexion/Inscription
                case "effectuerInscription":
                    return new EffectuerInscriptionAction();   
                case "effectuerConnexion":
                    return new EffectuerConnexionAction();
                case "effectuerDeconnexion":
                    return new EffectuerDeconnexionAction();
                    
                case "effectuerModificationCompagnie":
                    return new EffectuerModificationCompagnieAction();

//lire document               
                case "lireDocument":
                    return new LireDocumentAction();
                    
                default:
                    return new DefaultAction();

            }
        }
        
        return new DefaultAction();

    }
}
