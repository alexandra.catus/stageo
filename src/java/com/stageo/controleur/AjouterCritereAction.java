/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.controleur;

import com.stageo.model.Critere;
import com.stageo.model.CritereDAO;
import com.stageo.model.Utilisateur;
import com.stageo.model.UtilisateurDAO;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import jdbc.Config;
import jdbc.Connexion;

/**
 *
 * @author JP
 */
public class AjouterCritereAction  implements Action, RequestAware, RequirePRGAction, SessionAware, DataSender {
     private HttpServletRequest request;
    private HttpSession session;
    private HttpServletResponse response;
    private HashMap data;

    @Override
    public String execute() {
        

       
        
     
        String action = "*.do?tache=afficherPageProfil";
        //request.setAttribute("vue", "accueil.jsp");

             
             
          
        if((request.getParameter("critere") != null)){
             
             
            try{
                
                Connection cnx = Connexion.startConnection(Config.DB_USER,Config.DB_PWD,Config.URL,Config.DRIVER);
                session = request.getSession(true);
                 
                UtilisateurDAO dao = new UtilisateurDAO(cnx);
                Utilisateur user = dao.read((String) session.getAttribute("connecte"));
                
                CritereDAO critDao = new CritereDAO(cnx);
                Critere crit = new Critere(request.getParameter("critere"));
                
               
                    
           
             
               
                critDao.create(crit);
                
               System.out.println("Critere A ETE AJOUTER");       
            }
            catch(ClassNotFoundException e){
                System.out.println("Erreur dans le chargement du pilote :"+ e);
            } catch (SQLException ex) {
                Logger.getLogger(EffectuerPostulationStageAction.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return action;

    }

    @Override
    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }
    
    @Override
    public void setResponse(HttpServletResponse response) {
        this.response = response;
    }

    @Override
    public void setSession(HttpSession session) {
        this.session = session;
    }

    @Override
    public void setData(Map<String, Object> data) {
        this.data = (HashMap) data;
    }   
}


