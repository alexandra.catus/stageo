/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.controleur;

/**
 *
 * @author Dave
 */
import java.sql.Connection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import jdbc.Config;
import jdbc.Connexion;
import com.stageo.model.Utilisateur;
import com.stageo.model.UtilisateurDAO;
import com.util.Util;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


public class EffectuerInscriptionAction implements Action, RequestAware, SessionAware, RequirePRGAction, DataSender {
    //private HttpSession session;
    private HttpServletRequest request;
    private HttpSession session;
    private HttpServletResponse response;
    private HashMap data;

    @Override
    public String execute() {
        

        System.out.println("JE SUIS DANS EffectuerInscriptionAction.java !!!");
        
        
        boolean erreur = false;
        boolean creer_user = true;
        String action = "*.do?tache=afficherPageInscription";
        if(!erreur){
            try{
                String  courriel = request.getParameter("courriel"),
                        prenom = request.getParameter("prenom"),
                        nom = request.getParameter("nom"),
                        motPasse =  request.getParameter("motDePasse"),
                        role = request.getParameter("role");
                

                Connection cnx = Connexion.startConnection(Config.DB_USER,Config.DB_PWD,Config.URL,Config.DRIVER);
                UtilisateurDAO dao = new UtilisateurDAO(cnx);
                Utilisateur user = new Utilisateur();
                user.setEmail(courriel);
                user.setPrenom(prenom);
                user.setNom(nom);
                user.setMotDePasse(motPasse);
                user.setRole(role);
                
                if(courriel != null){
                    //faire vérification avec des findBy
                    if(dao.findByCourriel(courriel) != null ){
                        data.put("erreurCourriel","Ce courriel est déjà utilisé par un utilisateur");
                    }
                    else{
                        creer_user=dao.create(user);
                        if(creer_user){
                             user = dao.findByIdentifiantMotPasse(courriel, motPasse);
                             session = request.getSession(true);
                             session.setAttribute("connecte", user.getIdUtilisateur());
                             session.setAttribute("role", user.getRole());
                             //@Todo:s Changer tous le site pour en fonction de la ligne suivante
                             action = "*.do?tache=afficherPageProfil";
                            
                            }
                            
                        else{
                            data.put("erreurInscription","Problème de création du compte. Veuillez réessayer. Si le problème survient à répétition, contactez un administrateur.");
                        }
                    }
                }
                
            }
            catch(ClassNotFoundException e){
                System.out.println("Erreur dans le chargement du pilote :"+ e);
            } catch (SQLException ex) {
                Logger.getLogger(EffectuerInscriptionAction.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return action;

    }

    @Override
    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }
    
    @Override
    public void setResponse(HttpServletResponse response) {
        this.response = response;
    }

    @Override
    public void setSession(HttpSession session) {
        this.session = session;
    }

    @Override
    public void setData(Map<String, Object> data) {
        this.data = (HashMap) data;
    }   
}
