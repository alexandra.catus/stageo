/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stageo.controleur;

import com.stageo.model.*;

import com.stageo.model.Utilisateur;
import com.stageo.model.UtilisateurDAO;
import com.util.Util;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import jdbc.Config;
import jdbc.Connexion;

/**
 *
 * @author Dave
 */
public class EffectuerModificationCompagnieAction implements Action, RequestAware, SessionAware, RequirePRGAction, DataSender{
    
    private HttpSession session;
    private HttpServletRequest request;
    private HttpServletResponse response;
    private HashMap data;
    
    @Override
    public String execute() {
        String action = "*.do?tache=afficherPageProfil";
       
            try {
                    Connection cnx = Connexion.startConnection(Config.DB_USER,Config.DB_PWD,Config.URL,Config.DRIVER);
                    
                    CompagnieDAO compagnieDao = new CompagnieDAO(cnx);
                    AdresseDAO adresseDao = new AdresseDAO(cnx);
                    
                    Compagnie compagnie = new Compagnie();
                    Adresse adresse = new Adresse();

                    
                    String  nom = request.getParameter("nom"),
                            siteWeb = request.getParameter("siteWeb"),
                            tel = request.getParameter("tel"),
                            noCivil =  request.getParameter("noCivil"),
                            rue = request.getParameter("rue"),
                            bureau =  request.getParameter("bureau"),
                            ville = request.getParameter("ville"),
                            codePostal =  request.getParameter("codePostal"),
                            province = request.getParameter("province"),
                            pays =  request.getParameter("pays");
                    
                    // Preparation de l'objet compagnie
                    compagnie.setNom(nom);
                    compagnie.setPageWeb(siteWeb);
                    
                    // Preparation de l'objet adresse
                    adresse.setIdAdresse(compagnie.getAdresse().getIdAdresse());
                    adresse.setNumeroCivique(noCivil);
                    adresse.setRue(rue);
                    adresse.setBureau(bureau);
                    adresse.setVille(ville);
                    adresse.setCodePostal(codePostal);
                    adresse.setProvince(province);
                    adresse.setPays(pays);
                    adresse.setTel(tel);
                    
                    // Création de l'adresse et de la compagnie
                    adresseDao.update(adresse);
                    compagnieDao.update(compagnie);
   
                } catch (ClassNotFoundException ex) {
                    System.out.println(ex);
                    Logger.getLogger(EffectuerModificationCompagnieAction.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (SQLException ex) {
                        System.out.println(ex);
                        Logger.getLogger(EffectuerModificationCompagnieAction.class.getName()).log(Level.SEVERE, null, ex);       
                        } finally {
                            Connexion.close();
                        }
                return action;
    }

    @Override
    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    @Override
    public void setResponse(HttpServletResponse response) {
        this.response = response;
    }

    @Override
    public void setSession(HttpSession session) {
        this.session = session;
    }

    @Override
    public void setData(Map<String, Object> data) {
        this.data = (HashMap) data;
    }
}
